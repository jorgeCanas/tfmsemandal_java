/**
 * 
 */
package context;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashSet;

import utils.IoFile;

/**
 * @author Juan Gal&aacute;n P&aacute;ez
 *
 */
public class Lattice {
	private LinkedHashSet<Concept> conceptLattice;
	private Concept top;
	private Concept bottom;
	
	/**
	 * Creates an empty Lattice.
	 */
	public Lattice(){
		this.conceptLattice=new LinkedHashSet<Concept>();
		this.top=null;
		this.bottom=null;
	}
	
	/**
	 * return A String describing the Lattice.
	 * @return A String describing the Lattice.
	 */
	public String toString(){
		String ret="BOTTOM: "+bottom.getId()+" TOP: "+top.getId()+"\n";
		ret+=this.conceptLattice.toString();
		return ret;
	}
	
	/**
	 * Clears the Lattice. The result is an empty Lattice.
	 */
	public void clear(){
		this.conceptLattice.clear();
		this.top=null;
		this.bottom=null;
	}

	/**
	 * return  The top Concept of the Lattice.
	 * @return  The top Concept of the Lattice.
	 */
	public Concept getTop() {
		return top;
	}

	/**
	 * Sets the top Concept.
	 * @param  top
	 */
	public void setTop(Concept top) {
		this.top = top;
	}

	/**
	 * return  The bottom Concept of the Lattice.
	 * @return  The bottom Concept of the Lattice.
	 */
	public Concept getBottom() {
		return bottom;
	}

	/**
	 * Sets the bottom Concept.
	 * @param  bottom
	 */
	public void setBottom(Concept bottom) {
		this.bottom = bottom;
	}
	
	/**
	 * Adds Concept 'c' to the Lattice.
	 * @param c
	 */
	public void addConcept(Concept c){
		this.conceptLattice.add(c);
	}
	
	/**
	 * return The number of Concepts in the Lattice.
	 * @return The number of Concepts in the Lattice.
	 */
	public int size() {
		return this.conceptLattice.size();
	}
	
	/**
	 * return The Concept which has the Attribute 'name' as own Attribute or null if there isn't any.
	 * @param name The name of an Attribute.
	 * @return The Concept which has the Attribute 'name' as own Attribute or null if there isn't any.
	 */
	public Concept getConceptByOwnAttribute(String name){
		Concept con;
		Iterator<Concept> it=this.conceptLattice.iterator();
		while(it.hasNext()){
			con=it.next();
			if(con.hasOwnAttribute(name))
				return con;
		}
		return null;
	}
	
	/**
	 * return The Concept which has the Object 'name' as own Object or null if there isn't any.
	 * @param name The name of an Object.
	 * @return The Concept which has the Object 'name' as own Object or null if there isn't any.
	 */
	public Concept getConceptByOwnObject(String name){
		Concept con;
		Iterator<Concept> it=this.conceptLattice.iterator();
		while(it.hasNext()){
			con=it.next();
			if(con.hasOwnObject(name))
				return con;
		}
		return null;
	}
	
	/**
	 * return Whether the concept given by its own attribute 'at1' subsumes or not the concept given
	 * by its own attribute 'at2'.
	 * @param at1
	 * @param at2
	 * @return Whether the concept given by its own attribute 'at1' subsumes or not the concept given
	 * by its own attribute 'at2'.
	 */
	public boolean subsume(String at1, String at2){
		Concept c1=this.getConceptByOwnAttribute(at1);
		Concept c2=this.getConceptByOwnAttribute(at2);
		if(c1!=null&&c2!=null)
			return c1.subsume(c2);
		else
			return false;
	}
	
	/**
	 * return Whether the two concepts give by its own attributes 'at1' and 'at2' respectively are disjoint.
	 * @param at1
	 * @param at2
	 * @return Whether the two concepts give by its own attributes 'at1' and 'at2' respectively are disjoint.
	 */
	public boolean disjoint(String at1, String at2){
		Concept c1=this.getConceptByOwnAttribute(at1);
		Concept c2=this.getConceptByOwnAttribute(at2);
		if(c1!=null&&c2!=null)
			return c1.disjoint(c2);
		else
			return false;
	}
	
	/**
	 * return Whether the two concepts give by its own attributes 'at1' and 'at2' respectively are equal.
	 * @param at1
	 * @param at2
	 * @return Whether the two concepts give by its own attributes 'at1' and 'at2' respectively are equal.
	 */
	public boolean equalc(String at1, String at2){
		Concept c1=this.getConceptByOwnAttribute(at1);
		Concept c2=this.getConceptByOwnAttribute(at2);
		if(c1!=null&&c2!=null)
			return c1.equals(c2);
		else
			return false;
	}
	
	/**
	 * Saves in a GraphViz file a graph representation of the Lattice. The file will be saved
	 * in the given path 'pathName'.
	 * @param pathName
	 * @throws IOException 
	 */
	//modificado.añadido directed.
	public void toVizFile(String pathName, boolean directed) throws IOException {
		if(directed)
			IoFile.saveTextFile(pathName, this.toVizDotSourceDigraph());
		else
			IoFile.saveTextFile(pathName, this.toVizDotSource());
	}
	
	
	private String toVizDotSource() {
		StringBuffer ret=new StringBuffer();
		ret.append("graph lattice {\n");
		ret.append("\tgraph [rankdir = \"TB\"];\n");
		Iterator<Concept> itc=this.conceptLattice.iterator();
		Concept c;
		while(itc.hasNext()){
			c=itc.next();
			ret.append("\tC" + c.getId()+" [label=\"C" + c.getOwnAttributes().toString() + "\"];\n");
			Iterator<Concept> hijos=c.getChildren().iterator();
			while(hijos.hasNext()){
				ret.append("\tC" + c.getId() + " -- C" + hijos.next().getId() + ";\n");
			}
		}
		ret.append("}\n");
		return ret.toString();
	}

	private String toVizDotSourceDigraph() {
		StringBuffer ret=new StringBuffer();
		ret.append("digraph lattice {\n");
		ret.append("\tgraph [rankdir = \"TB\"];\n");
		Iterator<Concept> itc=this.conceptLattice.iterator();
		Concept c;
		while(itc.hasNext()){
			c=itc.next();
			ret.append("\tC" + c.getId()+" [label=\"C" + c.getOwnAttributes().toString() + "\"];\n");
			Iterator<Concept> itch=c.getChildren().iterator();
			while(itch.hasNext()){
				Concept ch=itch.next();
				if(c.subsume(ch))
					ret.append("\tC" + c.getId() + " -> C" + ch.getId() + ";\n");
				else // Esto del error hay que tratarlo. O quitarlo ya que no deberia de darse.
					System.out.println("Error "+c.toString()+" hijo "+ch.toString());
			}
		}
		ret.append("}\n");
		return ret.toString();
	}
	
	//DEPURACION.
	/**
	 * 
	 * @deprecated
	 */
	public LinkedHashSet<Concept> getConcepts(){
		return this.conceptLattice;
	}
	
	/**
	 * 
	 * @deprecated
	 */
	public Hashtable<Integer,Integer> getExtenSize(){
		Hashtable<Integer,Integer> ret=new Hashtable<Integer,Integer>();
		Iterator<Concept> itc=this.conceptLattice.iterator();
		Concept c;
		int degree;
		Integer nConcepts;
		while(itc.hasNext()){
			c=itc.next();
			degree=c.getExtension().size();
			nConcepts=ret.get(degree);
			if(nConcepts==null)
				ret.put(degree, 1);
			else
				ret.put(degree, nConcepts+1);
		}
		return ret;
	}
	
	/**
	 * 
	 * @deprecated
	 */
	public Hashtable<Integer,Integer> getIntenSize(){
		Hashtable<Integer,Integer> ret=new Hashtable<Integer,Integer>();
		Iterator<Concept> itc=this.conceptLattice.iterator();
		Concept c;
		int degree;
		Integer nConcepts;
		while(itc.hasNext()){
			c=itc.next();
			degree=c.getIntension().size();
			nConcepts=ret.get(degree);
			if(nConcepts==null)
				ret.put(degree, 1);
			else
				ret.put(degree, nConcepts+1);
		}
		return ret;
	}
	
	/**
	 * 
	 * @deprecated
	 */
	public Hashtable<Integer,Integer> getLatticeConnectivity(){
		Hashtable<Integer,Integer> ret=new Hashtable<Integer,Integer>();
		Iterator<Concept> itc=this.conceptLattice.iterator();
		Concept c;
		int degree;
		Integer nConcepts;
		while(itc.hasNext()){
			c=itc.next();
			degree=c.getChildren().size()+c.getParents().size();
			nConcepts=ret.get(degree);
			if(nConcepts==null)
				ret.put(degree, 1);
			else
				ret.put(degree, nConcepts+1);
		}
		return ret;
	}
	
	/**
	 * 
	 * @deprecated
	 */
	public Hashtable<Integer,Integer> getLatticeConnectivityNoBottom(){
		Hashtable<Integer,Integer> ret=new Hashtable<Integer,Integer>();
		Iterator<Concept> itc=this.conceptLattice.iterator();
		Concept c;
		int degree;
		Integer nConcepts;
		while(itc.hasNext()){
			c=itc.next();
			if(c.getId()!=1){
				degree=c.getChildren().size()+c.getParents().size();
				nConcepts=ret.get(degree);
				if(nConcepts==null)
					ret.put(degree, 1);
				else
					ret.put(degree, nConcepts+1);
			}
		}
		return ret;
	}
	
	/**
	 * 
	 * @deprecated
	 */
	public Hashtable<Integer,Integer> getLatticeConnectivityIn(){
		Hashtable<Integer,Integer> ret=new Hashtable<Integer,Integer>();
		Iterator<Concept> itc=this.conceptLattice.iterator();
		Concept c;
		int degree;
		Integer nConcepts;
		while(itc.hasNext()){
			c=itc.next();
			degree=c.getParents().size();
			nConcepts=ret.get(degree);
			if(nConcepts==null)
				ret.put(degree, 1);
			else
				ret.put(degree, nConcepts+1);
		}
		return ret;
	}
	/**
	 * 
	 * @deprecated
	 */
	public Hashtable<Integer,Integer> getLatticeConnectivityOut(){
		Hashtable<Integer,Integer> ret=new Hashtable<Integer,Integer>();
		Iterator<Concept> itc=this.conceptLattice.iterator();
		Concept c;
		int degree;
		Integer nConcepts;
		while(itc.hasNext()){
			c=itc.next();
			degree=c.getChildren().size();
			nConcepts=ret.get(degree);
			if(nConcepts==null)
				ret.put(degree, 1);
			else
				ret.put(degree, nConcepts+1);
		}
		return ret;
	}
}

