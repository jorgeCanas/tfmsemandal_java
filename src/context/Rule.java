/**
 * 
 */
package context;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;

/**
 * @author Juan Gal&aacute;n P&aacute;ez
 *
 */
public class Rule implements Cloneable{
	private LinkedHashSet<String> premise;
	private LinkedHashSet<String> conclusion;
	private int support;
	private double confidence;
	
	/**
	 * Creates an empty Rule.
	 */
	public Rule(){
		this.premise=new  LinkedHashSet<String>(); 
		this.conclusion=new  LinkedHashSet<String>();
	}

	/**
	 * return  The support of the Rule.
	 * @return  The support of the Rule.
	 */
	public int getSupport() {
		return support;
	}

	/**
	 * Sets the support of the Rule.
	 * @param  support
	 */
	public void setSupport(int support) {
		this.support = support;
	}

	/**
	 * return  The confidence of the Rule. Range [0.0,1.0].
	 * @return  The confidence of the Rule. Range [0.0,1.0].
	 */
	public double getConfidence() {
		return confidence;
	}

	/**
	 * Sets the confidence of the Rule. Range [0.0,1.0].
	 * @param  confidence
	 */
	public void setConfidence(double confidence) {
		this.confidence = confidence;
	}
	
	/**
	 * return the conclusion of the rule.
	 * @return the conclusion of the rule.
	 */
	public LinkedHashSet<String> getConclusion() {
		return conclusion;
	}

	/**
	 * return the premise of the rule
	 * @return the premise of the rule
	 */
	public LinkedHashSet<String> getPremise() {
		return premise;
	}

	/**
	 * Adds the Attribute 'name' to the premise of the Rule.
	 * @param name
	 */
	public void addPremise(String name){
		this.premise.add(name);
	}
	
	/**
	 * Adds the Attribute 'name' to the conclusion of the Rule.
	 * @param name
	 */
	public void addConclusion(String name){
		this.conclusion.add(name);
	}
	
	/**
	 * return A String describing the Rule.
	 * @return A String describing the Rule.
	 */
	public String toString(){
		String ret="<"+this.support+">  ";
		ret+=this.premise.toString()+" -- " + (int)(this.confidence*100) + "% --> "+this.conclusion.toString()+"\n";		
		return ret;
	}
	
	/**
	 * 
	 * @param attributes
	 * @return Whether the set 'attributes' satisfies or not the Rule.
	 */
	public boolean satisfyRule(Collection<String> attributes) {
		//Modificar? str.contains("ETIQUETA_");
		return !this.satisfyPremise(attributes) || attributes.containsAll(conclusion);
	}
	
	/**
	 * return Whether the set 'attributes' satisfies or not the premise of the Rule.
	 * @param attributes
	 * @return Whether the set 'attributes' satisfies or not the premise of the Rule.
	 */
	public boolean satisfyPremise(Collection<String> attributes){
		return attributes.containsAll(this.premise);
	}
	
	public Object clone(){
		Rule ret=new Rule();
		Iterator<String> its=this.getPremise().iterator();
		while(its.hasNext())
			ret.addPremise(new String(its.next()));
		its=this.getConclusion().iterator();
		while(its.hasNext())
			ret.addConclusion(new String(its.next()));
		ret.setConfidence(this.getConfidence());
		ret.setSupport(this.getSupport());
		return ret;
	}
}
