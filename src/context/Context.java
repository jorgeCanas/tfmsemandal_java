/**
 * 
 */
package context;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;
import java.util.Scanner;

import conexp.core.AttributeInformationSupplier;
import conexp.core.ContextEntity;
import conexp.core.DependencySet;
import conexp.core.ExtendedContextEditingInterface;
import conexp.core.GenericDependency;
import conexp.core.Implication;
import conexp.core.ImplicationSet;
import conexp.core.LatticeElement;
import conexp.core.Set;
import conexp.core.associations.SimpleAssociationMiner;
import context.conexp.SetBuilder;

/**
 * @author Juan Gal&aacute;n P&aacute;ez
 *
 */
public class Context {
	private String name;
	private Hashtable<String,FCAElement> attributes;
	private Hashtable<String,FCAElement> objects;
	private boolean [] modified;
	private boolean rigid;
	private LinkedList<Rule> rules = new LinkedList<Rule>();
	private Lattice lattice=new Lattice();
	private ExtendedMatrix matrix=null;
	public final static int LATTICE=0;
	public final static int AR=1;
	public final static int SB=2;
	public final static int MAT=3;
	
	/**
	 * Creates an empty Context with name='_name'. By default the rigid property is set to false.
	 * @param _name
	 */
	public Context(String _name){
		this.name=_name;
		this.attributes=new Hashtable<String,FCAElement>();
		this.objects=new Hashtable<String,FCAElement>();
		this.modified=new boolean [4];
		this.resetModified();
		rigid=true;
	}
	/**
	 * Creates an empty Context with name='_name' and the property rigid='_rigid'.
	 * @param _name
	 * @param _rigid
	 */
	public Context(String _name, boolean _rigid){
		this(_name);
		this.rigid=_rigid;
	}
	
	public Context(String _name, String _path) throws FileNotFoundException{
		this.name=_name;
		this.modified=new boolean [4];
		loadContextFromCxtFile(_path);
		this.resetModified();
		rigid=true;
	}
	
	public Context(String _name, String _path, boolean _rigid) throws FileNotFoundException{
		this(_name,_path);
		this.rigid=_rigid;
	}
	
	public Context(String _name, int obj, int at, double density){
		this.name=_name;
		this.modified=new boolean [4];
		loadContextWithRandomData(obj,at,density);
		this.resetModified();
		rigid=true;
	}
	
	/**
	 * 
	 * @param _name
	 * @param obj
	 * @param at
	 * @param density
	 * @param _rigid
	 */
	public Context(String _name, int obj, int at, double density, boolean _rigid){
		this(_name,obj,at,density);
		this.rigid=_rigid;
	}
	
	
	// depurar.
	private int loadContextFromCxtFile(String path) throws FileNotFoundException{
		Scanner s = new Scanner(new File(path));
		this.attributes=new Hashtable<String,FCAElement>();
		this.objects=new Hashtable<String,FCAElement>();
		String l;
		l=s.nextLine().trim();
		if(!l.equals("B")){
			System.out.println("ERROR CXT1");
			s.close();
			return -1;
		}
		s.nextLine();
		int nObj=Integer.parseInt(s.nextLine());
		int nAtt=Integer.parseInt(s.nextLine());
		System.out.println("O: "+nObj+" A: "+nAtt);
		s.nextLine();
		
		ArrayList<String> lObj= new ArrayList<String>();
		ArrayList<String> lAtt= new ArrayList<String>();
		
		for(int i=0;i<nObj;i++){
			lObj.add(s.nextLine().trim());
		}
		for(int i=0;i<nAtt;i++){
			lAtt.add(s.nextLine().trim());
		}
		
//		System.out.println(lAtt);
		
		this.addAttributes(lAtt);
		
		for (int i=0;i<nObj;i++){
//			if(i%100==0){
//				System.out.println("Cont: "+i);
//			}
			l=s.nextLine().trim();
			if(l.length()!=nAtt){
				System.out.println("ERROR CXT2");
				s.close();
				return -1;
			}
			String obj=lObj.get(i);
			LinkedList<String> ls=new LinkedList<String>();
			for(int j=0;j<nAtt;j++){
				if(l.charAt(j)=='X')
					ls.add(lAtt.get(j));
			}
			this.addObject(obj, ls);
			
		}
		if(s.hasNextLine()&&(l=s.nextLine().trim()).length()>0){
			System.out.println("ERROR CXT3:");
			System.out.println(l);
			s.close();
			return -1;
		}
		s.close();
		return 1;
	}
	
	// depurar
	// Ojo la densidad es aproximada, no exacta.
		private void loadContextWithRandomData(int obj, int at, double density){
			this.attributes=new Hashtable<String,FCAElement>();
			this.objects=new Hashtable<String,FCAElement>();
			LinkedList<String> lat=new LinkedList<String>();
			for(int i=0;i<at;i++){
				lat.add("A"+i);
			}
			this.addAttributes(lat);
			Random r=new Random();
			for(int i=0;i<obj;i++){
				Iterator<String> it=lat.iterator();
				LinkedList<String>ato=new LinkedList<String>();
				while(it.hasNext()){
					String nat=it.next();
					if(r.nextDouble() < density){
						ato.add(nat);
					}
				}
				this.addObject("O"+i, ato);
			}
		}
	
	/**
	 * Adds the Attribute '_name' to the Context if another Attribute with the same name doesn't exist.
	 * @param _name
	 * @return True if finally the new Attribute has been added.
	 */
	public boolean addAttribute(String _name){
		if(attributes.containsKey(_name)){
			System.out.println("CXT ERROR: Duplicate attribute: "+_name);
			return false;
		}
		attributes.put(_name, new FCAElement (_name));
		this.resetModified();
		return true;
	}
	
	/**
	 * Adds a collection of Attributes to the Context.
	 * @param c
	 * @return True if all Attributes were added or false if at least one Attribute wasn't added.
	 */
	public boolean addAttributes(Collection<String> c){
		boolean ret=true;
		Iterator<String> it=c.iterator();
		while(it.hasNext()){
			ret &= this.addAttribute(it.next());
		}
		this.resetModified();
		return ret;
	}
	
	/**
	 * Adds the Object '_name' and the collection of attributes it owns 'c', to the
	 * Context, if another Objects with the same name doesn't exist. If an Attribute in 'c'
	 * doesn't belong to the Context, depending on the true/false value of the rigid property
	 * an error will be raised or the new attribute will be added to the Context respectively.
	 * @param _name
	 * @param c
	 * @return True if finally the new Object has been added.
	 */
	public boolean addObject(String _name, Collection<String> c){
		if(objects.containsKey(_name)){
			System.out.println("CXT ERROR: Duplicate object: "+_name);
			return false;
		}
		FCAElement obj=new FCAElement(_name);
		Iterator<String> it=c.iterator();
		while(it.hasNext()){
			this.setAttribute2Object(obj, it.next());
		}
		this.objects.put(_name, obj);
		this.resetModified();
		return true;
	}
	
	
	// revisar todo y añadir
		/**
		 * Depuracion
		 * 
		 */
	public void addAttributeToObject(String obj, String att){
		if(!this.objects.containsKey(obj)){
			LinkedList<String> latt=new LinkedList<String>();
			latt.add(att);
			this.addObject(obj, latt);
		}else{
			this.setAttribute2Object(this.getObject(obj), att);
			this.resetModified();
		}
	}
	
	//cualquier metodo que llame a este, debe poner modified=true.
	private boolean setAttribute2Object(FCAElement obj, String nat){
		if(!this.attributes.containsKey(nat)){
			if(rigid){
				System.out.println("CXT ERROR: Attribute '"+nat+"' doesn't exist");
				return false;
			}else
				this.addAttribute(nat);
		}
		FCAElement at=this.getAttribute(nat);
		at.addElement(obj);
		obj.addElement(at);
		return true;
	}
	
	/**
	 * Not implemented.
	 * @param name
	 */
	// added 26/02/2013. revisar
	public void removeAttribute(String name){
		this.resetModified();
		Collection<String> extent=this.getAttributeExtent(name);
		this.attributes.remove(name);
		for(String obj: extent){
			this.objects.get(obj).getElements().remove(name);
		}
	}
	
	/**
	 * Not implemented.
	 * @param name
	 */
	public void removeObject(String name){
		System.out.println("CXT ERROR: removeObject method doesn't exist.");
	}
	
	/**
	 * return  The name of the Context.
	 * @return  The name of the Context.
	 */
	public String getName() {
		return name;
	}

	/**
	 * return Whether the Context has been modified or not after the last
	 * time association rules and lattice were computed.
	 * @return Whether the Context has been modified or not after the last
	 * time association rules and lattice were computed.
	 */
	public boolean isModified(int index) {
		//System.out.println("isModified "+this.modified[index]);
		return this.modified[index];
	}

	private void setModified(int index, boolean modified) {
		this.modified[index] = modified;
	}
	
	private void resetModified() {
		this.setModified(LATTICE, true);
		this.setModified(AR, true);
		this.setModified(SB, true);
		this.setModified(MAT, true);
	}
	/**
	 * The true/false value of the rigid property.
	 * @return  The true/false value of the rigid property.
	 */
	public boolean isRigid() {
		return this.rigid;
	}

	/**
	 * Sets the true/false value of the rigid property.
	 * @param  rigid
	 */
	public void setRigid(boolean rigid) {
		this.rigid = rigid;
	}

	private FCAElement getAttribute(String _name){
		return this.attributes.get(_name);
	}
	
	private FCAElement getObject(String _name){
		return this.objects.get(_name);
	}
	
	//DEPURACION!!!
	/**
	 * Depuracion
	 * @deprecated
	 */
	public int existsRelation(String obj, String at) {
		FCAElement a=this.attributes.get(at);
		FCAElement o=this.objects.get(obj);
		if (a == null || o == null) {
			System.out.println("Error en existRelacion para Objeto: "+obj+" y Atributo: "+at);
			return 0;
		}
		int ret1 = a.getElement(obj)==null ? 0 : 1;
		int ret2 = o.getElement(at)==null ? 0 : 1;
		if(ret1!=ret2){
			System.out.println("Error en existRelacion para Objeto: "+obj+" y Atributo: "+at);
			return 0;
		}
		return ret1;
	}
	
	/**
	 * A String describing the Context.
	 * @return A String describing the Context.
	 */
	public String toString(){
		if(this.isModified(MAT))
			this.updateMatrix();
		return matrix.toString();
	}
	
	/**
	 * return The number of Objects within the Context.
	 * @return The number of Objects within the Context.
	 */
	public int sizeObjects(){
		return this.objects.size();
	}
	
	/**
	 * return The number of Attributes within the Context
	 * @return The number of Attributes within the Context
	 */
	public int sizeAttributes(){
		return this.attributes.size();
	}
	
	/**
	 * return the association rule set generated.
	 * @return the association rule set generated.
	 */
	public LinkedList<Rule> getRules() {
		if(this.isModified(AR))
			this.updateAssociationRules();
			//this.updateAssociationRulesSE();
		System.out.println("Total rules " + rules.size());
		return cleanRules(rules);
	}
	
	
	/**
	 * return the association rule set generated.
	 * @return the association rule set generated.
	 */
	public LinkedList<Rule> getStemBasis(int minSupport) {
		if(!this.isModified(AR))
			return this.getRules(minSupport, 1.0);
		if(this.isModified(SB))
			this.updateStemBasis();
		if(minSupport==0)
			return rules;
		else{
			LinkedList<Rule> ret=new LinkedList<Rule>();
			Iterator<Rule> it=this.rules.iterator();
			while(it.hasNext()){
				Rule r=it.next();
				if(r.getSupport()>=minSupport)
					ret.add(r);
			}
			return ret;
		}
	}
	
	/**
	 * return A subset of the association rules generated with confidence and support greater or
	 * equal than 'minSupport' and 'minConfidence' respectively.
	 * @param minSupport
	 * @param minConfidence
	 * @return A subset of the association rules generated with confidence and support greater or
	 * equal than 'minSupport' and 'minConfidence' respectively.
	 */
	public LinkedList<Rule> getRules(int minSupport, double minConfidence){
		if(this.isModified(AR)){
			if(!this.isModified(SB)&&minConfidence==1.0)
				return this.getStemBasis(minSupport);
			this.updateAssociationRules();
			
			//this.updateAssociationRulesSE();
		}
		if(minSupport==0&&minConfidence==0.0){
			//modificado
			System.out.println("Total rules " + rules.size());
			return cleanRules(rules);
		}else{
			LinkedList<Rule> ret=new LinkedList<Rule>();
			Iterator<Rule> it=this.rules.iterator();
			while(it.hasNext()){
				Rule r=it.next();
				if(r.getSupport()>=minSupport&&r.getConfidence()>=minConfidence)
					ret.add(r);
			}
			//modificado
			System.out.println("Total rules " + ret.size());
			return cleanRules(ret);
		}
	}
	
	/**
	 * return The lattice associated to the Formal Context.
	 * @return The lattice associated to the Formal Context.
	 */
	public Lattice getLattice() {
		if(this.isModified(LATTICE))
			this.updateLattice();
		return lattice;
	}
	
	private void clearRules(){
		this.rules.clear();
	}
	
	private void addRule(Rule r){
		this.rules.add(r);
	}
	
	//DEPURACION!!
	public int [][] getMatrix(){
		if(this.isModified(MAT))
			this.updateMatrix();
		return this.matrix.matrix;
	}
	
	public String [] getAtrNames(){
		if(this.isModified(MAT))
			this.updateMatrix();
		return this.matrix.atNames;
	}
	
	public String [] getObjNames(){
		if(this.isModified(MAT))
			this.updateMatrix();
		return this.matrix.objNames;
	}
	
	//HAY QUE AÑADIRLO
	/**
	 * Depuracion
	 * 
	 */
	public int getNumObjects(String attribute){
		FCAElement at=this.getAttribute(attribute);
		if(at==null)
			return -1;
		else
			return at.getNumElements();
	}
	
	//Revisar(faltan excepciones) y añadir
	public Collection<String> getObjectIntent(String obj){
		return this.objects.get(obj).elements.keySet();
	}
	
	//Revisar(faltan excepciones) y añadir
	public Collection<String> getAttributeExtent(String att){
		return this.attributes.get(att).elements.keySet();
	}
	
	//HAY QUE AÑADIRLO para objetos y atributos
	/**
	 * Depuracion
	 * 
	 */
	public boolean containsObject(String object){
		return this.objects.containsKey(object);
	}
	
//	 HAY QUE AÑADIRLO para objetos y atributos
	/**
	 * Depuracion
	 * 
	 */
	public boolean containsAttribute(String attribute){
		return this.attributes.containsKey(attribute);
	}
	
	// HAY QUE AÑADIRLO
	/**
	 * Depuracion
	 * 
	 */
	public int getNumAttributes(String object){
		FCAElement obj=this.getObject(object);
		if(obj==null)
			return -1;
		else
			return obj.getNumElements();
	}
	/**
	 * Depuracion
	 * @deprecated
	 */
	public Enumeration<String> getAtrNamesEnum(){
		return this.attributes.keys();
	}
	
	/**
	 * Depuracion
	 * @deprecated
	 */
	public Enumeration<String> getObjNamesEnum(){
		return this.objects.keys();
	}
	
	/**
	 * Depuracion
	 * 
	 */
	public Collection<String> getAtrNamesCol(){
		return this.attributes.keySet();
	}
	
	/**
	 * Depuracion
	 * 
	 */
	public Collection<String> getObjNamesCol(){
		return this.objects.keySet();
	}
	
	/**
	 * Depuracion
	 * @deprecated
	 */
	public int getObjectsSize(){
		return this.objects.size();
	}
	
	/**
	 * Depuracion
	 * @deprecated
	 */
	public int getAttributesSize(){
		return this.attributes.size();
	}
	//FIN DEPURACION.
	
	
	/**
	 * Saves the Context in a ConExp file (.cxt). The
	 * will be saved in the path given by 'pathName'.
	 * @throws IOException 
	 */
	
	public void toConExp(String pathName) throws IOException{
		FileWriter f1 = new FileWriter(pathName);
	    PrintWriter pw = new PrintWriter(f1);

	    pw.print("B\n\n");
	    pw.print(this.getObjNames().length+"\n");
	    pw.print(this.getAtrNames().length+"\n\n");
		for(int i=0;i<this.getObjNames().length;i++){
			pw.print(this.getObjNames()[i]+"\n");
		}
		for(int i=0;i<this.getAtrNames().length;i++){
			pw.print(this.getAtrNames()[i]+"\n");
		}
		for(int i=0;i<this.getObjNames().length;i++){
			for(int j=0;j<this.getAtrNames().length;j++){
				pw.print(this.getMatrix()[i][j]==0 ? "." : "X");
			}
			pw.print("\n");
		}
		pw.println();
		f1.close();	
	}
	
//	\begin{tabular}{l|p{8mm}p{8mm}p{8mm}p{8mm}p{8mm}p{8mm}|} \centering
//	& A1 & A2 & A3 & A4 & A5 & A6 \\
//	\hline
//	\rowcolor[gray]{.9} Obj1 & \e & \e & & & \e & \\
//	Obj2 & \e & \e & & & \e & \\
//	\rowcolor[gray]{.9} Obj3 & \e & \e & & & & \\
//	Obj4 & \e & \e & & & & \\
//	\rowcolor[gray]{.9} Obj5 & \e & & & & & \\
//	Obj6 & \e & & & & & \\
//	\rowcolor[gray]{.9} Obj7 & \e & & & & & \\
//	Obj8 & & & & \e & & \\
//	\rowcolor[gray]{.9} Obj9 & & & & \e & & \\
//	Obj10 & & & & \e & & \\
//	\rowcolor[gray]{.9} Obj11 & & & \e & \e & & \\
//	Obj12 & & & \e & \e & & \\
//	\rowcolor[gray]{.9} Obj13 & & & \e & \e & & \\
//	Obj14 & & & \e & \e & & \e \\
//	\rowcolor[gray]{.9} Obj15 & & & \e & \e & & \e \\
//	\hline
//	\end{tabular}

	// Codigo LATEX: "\e" y Error con rowcolor, buscar paquete.
	// decidir los parametros para el calculo del ancho de las columnas.
	/**
	 * Generates the LaTeX code of a table representation of the Context.
	 * @return A String with the LaTeX code of a table representation of the Context.
	 */
	public String toTeX(){
		StringBuffer ret = new StringBuffer();
		//ret.append("\\begin{table}");
		String head1="\\begin{tabular}{l|";
		String head2="";
		for(int i=0;i<this.getAtrNames().length;i++){
			head1+="p{8mm}";//El numero de p{8mm} no seria i+1?? por aquello de la columna de nombres de objetos?
			head2+=" & "+this.getAtrNames()[i];
		}
		ret.append(head1+"|} \\centering\n"+head2+" \\\\\n");
		ret.append("\\hline\n");
		String row;
		for(int i=0;i<this.getObjNames().length;i++){
			row="";
			if(i%2==0){
				row+="\\rowcolor[gray]{.9} ";
			}
			row+=this.getObjNames()[i]+" ";
			for(int j=0;j<this.getAtrNames().length;j++){
				row+="& "+(this.getMatrix()[i][j]==0 ? "" : "$\\times$ ");
			}
			ret.append(row+"\\\\\n");
		}
		ret.append("\\hline\n");
		ret.append("\\end{tabular}");
		//ret.append("\\end{table}");
		return ret.toString();
	}
	
	//----------------------------------------------------------------------------------
	//-------------------PRODUCTION-SYSTEM----------------------------------------------
	//----------------------------------------------------------------------------------
	/**
	 * Computes the initial confidence for a given fact 'attribute'. (Ocurrences+1/NumObjects+1).
	 * @return The double value of the initial confidence.
	 */
	public double getFactInitialConfidence(String attribute){
		FCAElement atr=this.getAttribute(attribute);
		if(atr==null){
			return -1;//El tratamiento del error queda delegado a ProductionSystem.
		}
		double ocurrences=atr.getNumElements()+1;
		double numObjects=this.sizeObjects()+1;
		return ocurrences/numObjects;
	}
	
	//Nueva version que busca la frecuencia del conjunto de hechos al completo. Añadido 25/02/2013.
	/**
	 * @return 
	 */
	public double getFactsFrequency(Collection<String> facts, Collection<String> classes){
		if(facts==null || classes==null){
			return -1;//El tratamiento del error queda delegado a ProductionSystem.
		}
		double ocurrences=1;
		for(FCAElement obj : objects.values()){
			LinkedList<String> atts=new LinkedList<String>();
			for(String att : obj.elements.keySet()){
				if(!classes.contains(att))
					atts.add(att);
			}
			if(atts.containsAll(facts)&&facts.containsAll(atts))
				ocurrences+=1;
		}
		double numObjects=this.sizeObjects()+1;
		System.out.println("occurrences: "+ocurrences+" objects: "+numObjects+" freq: "+(ocurrences/numObjects));
		return ocurrences/numObjects;
	}
	
	//----------------------------------------------------------------------------------
	//----------------------------------------------------------------------------------
	
	private class ExtendedMatrix {
			private int [][] matrix;
			private String [] atNames;
			private String [] objNames;
					
			public ExtendedMatrix(){
				this.matrix=new int [objects.size()][attributes.size()];
				Object [] atNamesAux=attributes.keySet().toArray();
				this.atNames=new String [atNamesAux.length];
				for(int i=0;i<atNamesAux.length;i++){
					this.atNames[i]=(String)atNamesAux[i];
				}
				Object [] objectsAux=objects.values().toArray();
				this.objNames=new String [objectsAux.length];
				for (int i=0;i<objectsAux.length;i++){
					this.objNames[i]=((FCAElement)objectsAux[i]).getName();
				}
				for(int i=0;i<matrix.length;i++)
					for(int j=0;j<matrix[i].length;j++){
						this.matrix[i][j]= (((FCAElement) objectsAux[i]).containsElement(atNames[j])) ? 1 : 0;
					}
			}
			
			public String toString(){
				StringBuffer sb=new StringBuffer();
				for(int i =0;i<this.atNames.length;i++){
					sb.append("\t"+this.atNames[i]);
				}
				sb.append("\n");
				for(int i=0;i<this.objNames.length;i++){
					sb.append(this.objNames[i]);
					for(int j=0;j<this.atNames.length;j++){
						sb.append("\t"+(this.matrix[i][j]==0 ? "." : "X"));
					}
					sb.append("\n");
				}
				return sb.toString();
			}
		}
	
	private class FCAElement {
		private String name;
		private Hashtable<String,FCAElement> elements;
		
		/**
		 * Creates an empty FCAElement with name='name'.
		 * @param name
		 */
		public FCAElement(String name){
			this.name=name;
			elements=new Hashtable<String,FCAElement>();
		}
		
		/**
		 * return  The name of the FCAElement.
		 * @return  The name of the FCAElement.
		 */
		public String getName() {
			return name;
		}

		/**
		 * return The elements of this FCAElement.
		 * @return The elements of this FCAElement.
		 */
		public Hashtable<String,FCAElement> getElements() {
			return elements;
		}
		
		/**
		 * return The FCAElement corresponding with the given name.
		 * @param element The name of an FCAElement.
		 * @return The FCAElement corresponding with the given name.
		 */
		public FCAElement getElement(String element){
			return this.elements.get(element);
		}

		/**
		 * Adds an element 'e' to this FCAElement.
		 * @param e
		 */
		public void addElement(FCAElement e){
			this.elements.put(e.name,e);
		}
		
		/**
		 * Adds a collection of elements to this FCAElement.
		 * @param c A collection of FCAElements
		 */
		public void addElements(Collection<FCAElement> c){
			Iterator<FCAElement> it=c.iterator();
			while(it.hasNext()){
				FCAElement e=it.next();
				this.elements.put(e.name, e);
			}
		}
		
		/**
		 * return Whether this FCAElement contains or not the element 'name' within its elements.
		 * @param name The name of an element.
		 * @return Whether this FCAElement contains or not the element 'name' within its elements.
		 */
		public boolean containsElement(String name){
			return this.elements.containsKey(name);
		}
		
		/**
		 * return The number of elements within this FCAElement.
		 * @return The number of elements within this FCAElement.
		 */
		public int getNumElements() {
			return this.elements.size();
		}
	}
	
	private void updateAssociationRules(){
		ExtendedContextEditingInterface cxt = SetBuilder.makeContext(this.getObjNames(), this.getAtrNames(), this.getMatrix());
		//UPDATE RULES
		this.clearRules();
		DependencySet cover = new DependencySet((AttributeInformationSupplier) cxt);
		SimpleAssociationMiner conj  = new SimpleAssociationMiner();
		conj.setContext((conexp.core.Context) cxt);
        conj.findAssociations(cover,0,0);
        	
		Iterator it1 =  cover.dependencies().iterator();
		while (it1.hasNext()) {
			GenericDependency gd = (GenericDependency)it1.next();
			
			Rule r=new Rule();
			
			r.setSupport(gd.getRuleSupport());
			r.setConfidence(gd.getConfidence());
			//premise.
			Set mip = gd.getPremise();
			for(int i=0;i<mip.size();i++) {
				if (mip.in(i)) {
					String nat= cxt.getAttribute(i).getName();
					r.addPremise(nat);
				}
			}
			//conclusion.
			Set mic = gd.getConclusion();
			for(int j=0;j<mic.size();j++) {
				if (mic.in(j)) {
					String nobj=cxt.getAttribute(j).getName();
					r.addConclusion(nobj);
				}
			}
			this.addRule(r);
		}
		
		this.setModified(AR,false);
		this.setModified(SB,false);
	}
	
	private void updateStemBasis(){
		ExtendedContextEditingInterface cxt = SetBuilder.makeContext(this.getObjNames(), this.getAtrNames(), this.getMatrix());
		//UPDATE STEM BASIS
		this.clearRules();
		DependencySet cover = new DependencySet((AttributeInformationSupplier) cxt);
		SimpleAssociationMiner conj  = new SimpleAssociationMiner();
		conj.setContext((conexp.core.Context)cxt);
		conj.findAssociations(cover,1,1.0);
		ImplicationSet implica = conj.getImplicationBase();
		Iterator it1 =  implica.iterator();
		while (it1.hasNext()) {
			Implication im = (Implication)it1.next();
			
			Rule r = new Rule();

			r.setSupport(im.getRuleSupport());
			r.setConfidence(im.getConfidence());
			//premise.
			Set mip = im.getPremise();
			for(int i=0;i<mip.size();i++) {
				if (mip.in(i)) {
					String nat= cxt.getAttribute(i).getName();
					r.addPremise(nat);
				}
			}
			//conclusion.
			Set mic = im.getConclusion();
			for(int j=0;j<mic.size();j++) {
				if (mic.in(j)) {
					String nobj=cxt.getAttribute(j).getName();
					r.addConclusion(nobj);
				}
			}
			this.addRule(r);
		}
		this.setModified(SB,false);
	}
	
	private void updateLattice(){
		ExtendedContextEditingInterface cxt = SetBuilder.makeContext(this.getObjNames(), this.getAtrNames(), this.getMatrix());
		//UPDATE LATTICE
		conexp.core.Lattice lat = SetBuilder.makeLattice((conexp.core.Context) cxt);
		LatticeElement le;
		Concept con;
		Hashtable<Integer,Concept> acum=new Hashtable<Integer,Concept>();
		//Primera pasada: Crear los conceptos y sus elementos.
		for (int i=0;i<lat.conceptsCount();i++){
			le=lat.elementAt(i);
			con=new Concept(le.getIndex());
			//own attributes
			if (le.hasOwnAttribs()) {
				Iterator it = le.ownAttribsIterator();
				con.addOwnAttribute(((ContextEntity)it.next()).getName());	
			}
			//own objects
			if (le.hasOwnObjects()) {
				Iterator it = le.ownObjectsIterator();
				con.addOwnObject(((ContextEntity)it.next()).getName());	
			}
			//intension
			conexp.core.Set intent=le.getAttribs();
			for (int j=0;j<intent.length();j++){
				if(intent.in(j)){
					con.addIntensionAttribute(cxt.getAttribute(j).getName());
				}
			}
			//extension
			conexp.core.Set extent=le.getObjects();
			for (int j=0;j<extent.length();j++){
				if(extent.in(j)){
					con.addExtensionObject(cxt.getObject(j).getName());
				}
			}
			acum.put(con.getId(), con);
		}
		//Segunda pasada: Crear relaciones entre conceptos.
		for (int i=0;i<lat.conceptsCount();i++){
			le=lat.elementAt(i);
			con=acum.get(i);
			//children
			Iterator it=le.getChildren().iterator();
			while(it.hasNext()){
				conexp.core.Concept c=(conexp.core.Concept)it.next();
				con.addChild(acum.get(c.getIndex()));
			}
			//parents
			it=le.getParents().iterator();
			while(it.hasNext()){
				conexp.core.Concept c=(conexp.core.Concept)it.next();
				con.addParent(acum.get(c.getIndex()));
			}
		}
		//Copiar todo a la estructura de datos final.
		this.lattice.clear();
		Enumeration<Concept> en=acum.elements();
		while(en.hasMoreElements()){
			this.lattice.addConcept(en.nextElement());
		}
		this.lattice.setBottom(acum.get(lat.getBottom().getIndex()));
		this.lattice.setTop(acum.get(lat.getTop().getIndex()));
		
		this.setModified(LATTICE,false);
	}
	
	private void updateMatrix(){
		this.matrix=new ExtendedMatrix();
		this.setModified(MAT, false);
	}

	//Added
	private LinkedList<Rule> cleanRules(LinkedList<Rule> rules) {
		LinkedList<Rule> clean = new LinkedList<Rule>(); 
		for(Rule r: rules){
			//System.out.println(r);
			boolean premiseCategory = false;
			boolean conclusionCategory = false;
			for(String p: r.getPremise()){
				if(p.contains("ETIQUETA_")){
					//System.out.println("Premisa con etiqueta "+p);
					premiseCategory = true;
					break;
				}
			}
			if(!premiseCategory)
				for(String c: r.getConclusion()){
					if(c.contains("ETIQUETA_")){
						//System.out.println("Conclusion con etiqueta "+c);
						conclusionCategory = true;
						break;
					}
				}
			
			//Si no contiene etiqueta en el lado izquierdo y si en el derecho
			if(!premiseCategory && conclusionCategory)
				clean.add(r);
		}
		return clean;
	}
}
