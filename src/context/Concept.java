/**
 * 
 */
package context;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;

/**
 * @author Juan Gal&aacute;n P&aacute;ez
 *
 */
public class Concept implements  Cloneable{
	private int id;
	private LinkedHashSet<String> extension;
	private LinkedHashSet<String> intension;
	private LinkedHashSet<String> ownObjects;
	private LinkedHashSet<String> ownAttributes;
	private LinkedHashSet<Concept> parents;
	private LinkedHashSet<Concept> children;
	
	/**
	 * Creates an empty Concept with id='_id'.
	 * @param _id
	 */
	public Concept(int _id){
		this.id=_id;
		this.extension=new LinkedHashSet<String>();
		this.intension=new LinkedHashSet<String>();
		this.ownAttributes=new LinkedHashSet<String>();
		this.ownObjects=new LinkedHashSet<String>();
		this.parents=new LinkedHashSet<Concept>();
		this.children=new LinkedHashSet<Concept>();
	}
	
	/**
	 * Adds Concept 'c' as parent.
	 * @param c
	 */
	public void addParent(Concept c){
		this.parents.add(c);
	}
	
	/**
	 * Adds Concept 'c' as child.
	 * @param c
	 */
	public void addChild(Concept c){
		this.children.add(c);
	}
	
	/**
	 * Adds Object 'name' to the extension.
	 * @param name
	 */
	public void addExtensionObject(String name){
		this.extension.add(name);
	}
	
	/**
	 * Adds Attribute 'name' to the intension.
	 * @param name
	 */
	public void addIntensionAttribute(String name){
		this.intension.add(name);
	}
	
	/**
	 * Adds Object 'name' as own object.
	 * @param name
	 */
	public void addOwnObject(String name){
		this.ownObjects.add(name);
	}
	
	/**
	 * Adds Attribute 'name' as own Attribute.
	 * @param name
	 */
	public void addOwnAttribute(String name){
		this.ownAttributes.add(name);
	}

	/**
	 * return  The id of the Concept.
	 * @return  The id of the Concept.
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * return Whether the Attribute 'name' is within the own attributes of the Concept or not.
	 * @param name
	 * @return Whether the Attribute 'name' is within the own attributes of the Concept or not.
	 */
	public boolean hasOwnAttribute(String name){
		return this.ownAttributes.contains(name);
	}
	
	/**
	 * return Whether the Object 'name' is within the own objects of the Concept or not.
	 * @param name
	 * @return Whether the Object 'name' is within the own objects of the Concept or not.
	 */
	public boolean hasOwnObject(String name){
		return this.ownObjects.contains(name);
	}
	
	//
	/**
	 * Two Concepts are equal if the have same intension (or extension).
	 */
	public boolean equals(Object o){
		if(!(o instanceof Concept))
			return false;
		//else if(!(this.extension.equals(((Concept)o).extension)&&this.intension.equals(((Concept)o).intension)))
		else if(!(this.intension.equals(((Concept)o).intension)))
			return false;
		else 
			return true;
	}
	
	/**
	 * return Whether the Concept and the given concept 'c' are disjoint.
	 * @param c
	 * @return Whether the Concept and the given concept 'c' are disjoint.
	 */
	public boolean disjoint(Concept c){
		return Collections.disjoint(this.extension, c.extension);
	}
	
	/**
	 * return Whether the Concept subsumes or not the given concept 'c'.
	 * @param c
	 * @return Whether the Concept subsumes or not the given concept 'c'.
	 */
	public boolean subsume(Concept c){
		return this.extension.containsAll(c.extension);
	}

	/**
	 * return A String describing the Concept.
	 * @return A String describing the Concept.
	 */
	public String toString(){
		String ret="\nCONCEPT_"+id+"\n";
		ret+="EXTENSION: "+extension.toString()+"\n";
		ret+="INTENSION: "+intension.toString()+"\n";
		ret+="OWN_OBJECTS: "+this.ownObjects.toString()+"\n";
		ret+="OWN_ATTRIBUTES: "+this.ownAttributes.toString()+"\n";
		LinkedList<Integer> lid=new LinkedList<Integer>();
		Iterator<Concept> it=this.children.iterator();
		while(it.hasNext()){
			lid.add(it.next().getId());
		}
		ret+="CHILDREN: "+lid.toString()+"\n";
		lid=new LinkedList<Integer>();
		it=this.parents.iterator();
		while(it.hasNext()){
			lid.add(it.next().getId());
		}
		ret+="PARENTS: "+lid.toString()+"\n\n";
		return ret;
	}

	/**
	 * return the ownAttributes of the Concept.
	 * @return the ownAttributes of the Concept.
	 */
	public LinkedHashSet<String> getOwnAttributes() {
		return ownAttributes;
	}

	/**
	 * return the children of the Concept.
	 * @return the children of the Concept.
	 */
	public LinkedHashSet<Concept> getChildren() {
		return children;
	}

	/**
	 * @return the extension
	 */
	public LinkedHashSet<String> getExtension() {
		return extension;
	}

	/**
	 * @return the intension
	 */
	public LinkedHashSet<String> getIntension() {
		return intension;
	}

	/**
	 * @return the ownObjects
	 */
	public LinkedHashSet<String> getOwnObjects() {
		return ownObjects;
	}

	/**
	 * @return the parents
	 */
	public LinkedHashSet<Concept> getParents() {
		return parents;
	}
	
	//TODO OJO no se clonan los padres e hijos, si hiciera falta añadir.
	public Object clone(){
		Concept ret=new Concept(this.getId());
		ret.parents=null;
		ret.children=null;
		Iterator<String> its=this.getIntension().iterator();
		while(its.hasNext())
			ret.addIntensionAttribute(new String(its.next()));
		its=this.getExtension().iterator();
		while(its.hasNext())
			ret.addExtensionObject(new String(its.next()));
		its=this.getOwnAttributes().iterator();
		while(its.hasNext())
			ret.addOwnAttribute(new String(its.next()));
		its=this.getOwnObjects().iterator();
		while(its.hasNext())
			ret.addOwnObject(new String(its.next()));
		return ret;
	}
}

