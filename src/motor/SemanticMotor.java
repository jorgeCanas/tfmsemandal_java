package motor;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;

import context.Rule;

public class SemanticMotor {
	private LinkedList<Rule> rules;
	
	public SemanticMotor(LinkedList<Rule> rules) {
		System.out.println("SemanticMotor");
		this.rules = rules;
	}

	public LinkedList<Rule> getRules() {
		return rules;
	}


	public void setRules(LinkedList<Rule> rules) {
		this.rules = rules;
	}
	
	//public LinkedList<CategoryResult> getCategories(LinkedList<String> facts){
	public LinkedList<CategoryResult> getConclusionNames(LinkedList<String> facts){
		LinkedList<CategoryResult> result = new LinkedList<CategoryResult>();
		for(Rule r: this.getRules()){
			if (satisfyMotorRule(facts, r)) {
				//LinkedList<String> category = new LinkedList<String>();
				for (String s: r.getConclusion()){
					if (s.contains("ETIQUETA_")){
						//category.add(s);
						result.add(new CategoryResult(s, r));
					}
				}
			}
		}
		
		//Returns the category result ordered by best confidence
		result = this.getBestConfidence(result);
		Collections.sort(result, new CategoryResultComparator());
		return result;
	}
	
	public LinkedList<CategoryResult> getBestConfidence(LinkedList<CategoryResult> categoryResult){
		LinkedList<CategoryResult> cr = new LinkedList<CategoryResult>();
		Map<String, CategoryResult> hash = new HashMap<String, CategoryResult>();
		
		for(CategoryResult result: categoryResult){
			CategoryResult best = hash.get(result.getCategory());
			if(best != null){ //Hay un elemento en esta categoria
				//El elemento a introducir tiene mayor confianza que el almacenado
				if(best.getRule().getConfidence() < result.getRule().getConfidence()){
					hash.put(best.getCategory(), best);
				}
			}else{ //No hay elementos para esta categoria
				hash.put(result.getCategory(), result);
			}
		}
		
		Iterator it = hash.entrySet().iterator();
	    while (it.hasNext()) {
	    	Entry category = (Entry)it.next();
	        cr.add((CategoryResult)category.getValue());
	    }
		
		return cr;
	}
	
	public boolean satisfyMotorRule(Collection<String> attributes, Rule r) {
		LinkedList<String> conclusion = new LinkedList<String>();
		LinkedHashSet<String> premise = r.getPremise();
		for (String s: r.getConclusion()){
			if(!s.contains("ETIQUETA_")){
				conclusion.add(s);
			}
		}
		return attributes.containsAll(premise) && attributes.containsAll(conclusion);
		//return !attributes.containsAll(premise) || attributes.containsAll(conclusion);
	}
	
	public class CategoryResult{
		private  String category;
		private Rule rule;
		
		public CategoryResult() {
			category = "";
			rule = new Rule();
		}
		
		public CategoryResult(String category, Rule rule) {
			super();
			this.category = category;
			this.rule = rule;
		}
		public String getCategory() {
			return category;
		}
		public void setCategory(String category) {
			this.category = category;
		}
		public Rule getRule() {
			return rule;
		}
		public void setRule(Rule rule) {
			this.rule = rule;
		}
		
		public String toString(){
			return "Category: " + this.category + " Rule " + this.rule.toString();
		}
	}
	
	public class CategoryResultComparator implements Comparator<CategoryResult>{
        @Override
        public int compare(CategoryResult o1, CategoryResult o2) {
            if(o1.getRule().getConfidence() < o2.getRule().getConfidence()){
                return 1;
            }else{
                return -1;
            }
        }
    }
	
}