package motor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConfussionMatrix {
	Map<String, Map<String, Integer>> matrix = new HashMap<String, Map<String, Integer>>();
	
	public ConfussionMatrix(String[] labels){
		for(String label: labels){
			Map<String, Integer> innerMap = new HashMap<String, Integer>();
			for(String innerLabel: labels){
				innerMap.put(innerLabel, 0);				
			}
			matrix.put(label, innerMap);
		}
		//System.out.println(matrix);
		//matrix.get("etiqueta1").put("etiqueta2", 1);
		//System.out.println(matrix.get("etiqueta1").get("etiqueta2"));
	}
	
	public ConfussionMatrix(){}

	public Map<String, Map<String, Integer>> getMatrix() {
		return matrix;
	}
	
	public void setValue(String assignLabel, String givenLabel) {
		if(matrix.get(assignLabel) == null){
			Map<String, Integer> innerMap = new HashMap<String, Integer>();
			innerMap.put(givenLabel, 1);
			matrix.put(assignLabel, innerMap);
		}else if(matrix.get(assignLabel).get(givenLabel) == null){
			matrix.get(assignLabel).put(givenLabel, 1);
		}else{
			int value = matrix.get(assignLabel).get(givenLabel);
			matrix.get(assignLabel).put(givenLabel, value + 1);
		}
	}
	
	public int getTraza(){
		int traza = 0;
		for(String label: matrix.keySet())
			if(matrix.get(label).get(label) != null)
				traza += matrix.get(label).get(label);
		
		return traza;
	}
	
	public int getErrors(){
		int errors = 0;
		for(Map<String, Integer> innerMatrix: matrix.values())
			for(int value:innerMatrix.values())
				errors += value;
		
		return errors - this.getTraza();
	}
	
	public void setValues(List<String> assignLabels, List<String> givenLabels){
		if(givenLabels.size() > 0){
			for(String assignLabel: assignLabels){
				if(givenLabels.contains(assignLabel)){
					this.setValue(assignLabel, assignLabel);
					givenLabels.remove(assignLabel);
				}
			}
			for(String assignLabel: assignLabels)
				for(String givenLabel: givenLabels)
					this.setValue(assignLabel, givenLabel);
		}
	}
}
