package utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.LinkedList;

import context.Rule;

/**
 * @author Juan Gal&aacute;n P&aacute;ez
 *
 */
public class IoFile {
	
	public static void saveTextFile(String path, String content) throws IOException{
	    PrintWriter pw = new PrintWriter(new FileWriter(path));
        pw.print(content+"\n");
		pw.close();	
	}
	
	//Added
	/*
	 * @author Jorge Ca�as Est�vez
	 */
	public static void saveCSVFile(String path, LinkedList<Rule> rules){
		System.out.println("Numero de reglas a escribir "+rules.size());
		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(
				    new FileOutputStream(path), "UTF-8"));
			writer.append("Support;Confidence;Premise;Conclusion\n");
			
			for(Rule rule: rules){
				String line = "";
				line += String.valueOf(rule.getSupport()) + ';';
				line += String.valueOf(rule.getConfidence()) + ';';
				if(rule.getPremise().isEmpty()){
					line += ';';
				}
				for(String part: rule.getPremise()){
					line += part + ',';
				}
				line = line.substring(0, line.length() - 1) + ';';
				for(String part: rule.getConclusion()){
					line += part + ',';
				}
				line = line.substring(0, line.length() - 1);
				writer.append(line + '\n');
			}
			
			writer.close();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//Added
	/*
	 * @author Jorge Ca�as Est�vez
	 */
	public static LinkedList<Rule> getRulesFromCSV(String path){
		LinkedList<Rule> rules = new LinkedList<Rule>();
		BufferedReader br = null;
		String line = "";
		String csvSplitBy = ";";

		try {
			br = new BufferedReader(new InputStreamReader(
                    new FileInputStream(path), "UTF-8"));
			String header = br.readLine();
			
			while ((line = br.readLine()) != null) {
				
				String[] ruleParts = line.split(csvSplitBy);
				
				Rule r = new Rule();
				r.setSupport(Integer.valueOf(ruleParts[0]));
				r.setConfidence(Double.valueOf(ruleParts[1]));
				String[] premises = ruleParts[2].split(",");
				for(String premise: premises){
					r.addPremise(premise);
				}
				String[] conclusions = ruleParts[3].split(",");
				for(String conclusion: conclusions){
					r.addConclusion(conclusion);
				}
				rules.add(r);
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return rules;
	}
}
