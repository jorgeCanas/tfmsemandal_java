package productionSystem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Set;

import context.Context;
import context.Rule;

/**
 *  @author Juan Gal&aacute;n P&aacute;ez
 *
 */
public class ProductionSystemSE implements IProductionSystem{
	private Context context;
	private double minConfidence;
	private int minSupport;
	private Hashtable<String,Double> conclusions;
	private LinkedHashSet<String> facts;
	private boolean modified;
	private boolean initialConfidence;
	private double confidenceFactor;
	private LinkedList<Rule> rules;
	
	/*
	 * NOTA1: Se han fusionado los sistemas expertos con y sin confianza. Ya que el resultado es el mismo
	 * pero sin confianza. La diferencia es que el proceso con confianza es menos eficiente. En un futuro
	 * si se va a usar mucho el modo sin confianza, se pueden añadir los dos metodos, aqui mismo. Pero de momento
	 * se usa el mismo metodo y se devuelven los resultados sin confianza. En experimentos anteriores se ha visto
	 * que el tiempo del Sistema experto con confianza ha sido siempre el menor de los problemas, siendo este practicamente
	 * despreciable.
	 * 
	 * NOTA2: Para mantener la simplicidad de las cadenas y de FCAELement, no creamos ninguna estructura externa nueva.
	 * Todo interno, y similar a contex. existe un setFacts, el cual pone a falso un modified. A partir de ahí,
	 * para obtener la confianza para un atributo llamamos a getConfi(atr), si modified esta a true, lanzamos el SP.
	 */
	

	//Mantiene la base de reglas actualizada respecto del contexto.
	/**
	 * Generates an empty Production System given the associated Context and
	 * the thresholds for confidence and support.
	 * @param _context
	 * @param _minConfidence
	 * @param _minSupport
	 */
	public ProductionSystemSE(Context _context, double _minConfidence, int _minSupport){
		this.context=_context;
		this.rules=null;
		this.setMinConfidence(_minConfidence);
		this.setMinSupport(_minSupport);
		this.conclusions=new Hashtable<String,Double>();
		this.facts=new LinkedHashSet<String>();
		this.initialConfidence=true;
		this.setConfidenceFactor(1.0);
	}
	
	public ProductionSystemSE(Context _context, double _minConfidence, int _minSupport, double confidenceFactor){
		this(_context,_minConfidence,_minSupport);
		this.setConfidenceFactor(confidenceFactor);
	}
	
	public ProductionSystemSE(Context _context, double _minConfidence, int _minSupport, boolean initialConfidence){
		this.context=_context;
		this.rules=null;
		this.setMinConfidence(_minConfidence);
		this.setMinSupport(_minSupport);
		this.conclusions=new Hashtable<String,Double>();
		this.facts=new LinkedHashSet<String>();
		this.initialConfidence=initialConfidence;
		this.setConfidenceFactor(1.0);
	}
	
	public ProductionSystemSE(Context _context, double _minConfidence, int _minSupport, boolean initialConfidence, double confidenceFactor){
		this(_context,_minConfidence,_minSupport,initialConfidence);
		this.setConfidenceFactor(confidenceFactor);
	}
	
	// Añadida alternativa para usar directamente reglas en vez de contexto. Todavia puede que no sea estable.
	public ProductionSystemSE(LinkedList<Rule> rules, double _minConfidence, int _minSupport){
		this.rules=rules;
		this.context=null;
		this.setMinConfidence(_minConfidence);
		this.setMinSupport(_minSupport);
		this.conclusions=new Hashtable<String,Double>();
		this.facts=new LinkedHashSet<String>();
		this.initialConfidence=false;//Solo de esta forma no se necesita Contexto alguno.
		this.setConfidenceFactor(1.0);
	}
	
	public ProductionSystemSE(LinkedList<Rule> rules, double _minConfidence, int _minSupport, double confidenceFactor){
		this(rules,_minConfidence,_minSupport);
		this.setConfidenceFactor(confidenceFactor);
	}
	
	public ProductionSystemSE(LinkedList<Rule> rules){
		this(rules,1.0,1);
	}
	
	/**
	 * Generates an empty Production System given the associated Context. Thresholds for confidence 
	 * and support are set by default to '1.0' and '0' respectively (Stem Basis).
	 * @param _context
	 */
	public ProductionSystemSE(Context _context){
		this(_context,1.0,1);
	}
	
	public ProductionSystemSE(Context _context,boolean initialConfidence){
		this(_context,1.0,1,initialConfidence);
	}
	
	
	//Aqui se recalcula el contexto. Asi se mantiene la correlacion de modificaciones.
	public LinkedList<Rule> getRules(){
		if(this.context==null){
			return new LinkedList<Rule>(this.rules);
		}else
			return this.context.getRules(this.getMinSupport(), this.getMinConfidence());
	}
	
	/**
	 * Return the facts loaded in the Production System. Those will be the initial facts
	 * in the inference process.
	 * @return the facts
	 */
	public LinkedHashSet<String> getFacts() {
		return facts;
	}

	/**
	 * Sets the initial facts for the inference process.
	 * @param attributes The facts collection
	 */
	public void setFacts(Collection<String> attributes) {
		this.setModified(true);
		this.facts = new LinkedHashSet<String>();
		this.facts.addAll(attributes);
	}
	
	//Si la conclusion no existe devuelve 0.
	/**
	 * Query the result of the inference process.
	 * @param attribute The conclusion to query.
	 * @return The confidence for the conclusion 'attribute' or 0 if 'attribute' is not
	 * within the Production System entailment.
	 */
	public double getConfidence(String attribute){
		if(this.isModified()){
			this.executionAllStep();
		}
		if(this.conclusions.get(attribute)==null)
			return 0;
		return this.conclusions.get(attribute);
	}
	
	/**
	 * return The names of all the conclusions inferred.
	 * @return The names of all the conclusions inferred.
	 */
	/*
	public LinkedList<String> getConclusionNames(){
		if(this.isModified())
			this.executionAllStep();
		LinkedList<String> ret= new LinkedList<String>();
		ret.addAll(this.conclusions.keySet());
		ret.removeAll(facts);
		return ret;
	}
	*/
	
	//Modificado
	public LinkedList<String> getConclusionNames(){
		if(this.isModified()){
			if(this.facts.isEmpty())
				System.out.println("PS WARNING: No facts have been added to the Production System.");
			this.initialize();
			LinkedList<Rule> rules=this.getRules();
			Hashtable<String,Double> facts=new Hashtable<String,Double>();
			Iterator<Entry<String,Double>> it=this.conclusions.entrySet().iterator();
			while(it.hasNext()){
				Entry<String,Double> en=it.next();
				facts.put(new String(en.getKey()), new Double(en.getValue().doubleValue()));
			}
			this.executionOneStep(rules,facts);
			
			this.setModified(false);
		}
		LinkedList<String> ret= new LinkedList<String>();
		ret.addAll(this.conclusions.keySet());
		ret.removeAll(facts);
		return ret;
	}
	
	/**
	 * return Whether the given 'attributes' collection satisfies or not the rule basis.
	 * @param attributes
	 * @return Whether the given 'attributes' collection satisfies or not the rule basis.
	 */
	public boolean satisfyProductionSystem(Collection<String> attributes) {
		Iterator<Rule> it=this.getRules().iterator(); 
		Rule r;
		while (it.hasNext()) {
			r = it.next();
			if(!r.satisfyRule(attributes))
				return false;
		}
		return true;
	}
	
	//Restringimos el acceso a conclusions.
	private void setConfidenceExec(String attribute, double confidence){
		this.conclusions.put(attribute, confidence);
	}
	
	//Metodo para la ejecucion. No comprueba si se ha modificado.Si la conclusion no existe devuelve 0.
	private double getConfidenceExec(String attribute){
		if(this.conclusions.get(attribute)==null)
			return 0;
		return this.conclusions.get(attribute);
	}
	
	private void initialize(){
		this.conclusions=new Hashtable<String,Double>();
		Iterator<String> it = this.facts.iterator();
		String at;
		double conf;
		if(this.isInitialConfidence())
			while(it.hasNext()){
				at=it.next();
				conf=context.getFactInitialConfidence(at);
				//He tratado el error como un warning ya que en principio no afecta al proceso.
				if(conf==-1)
					System.out.println("PS WARNING: Fact "+at+" doesn't exist in the current Context, thus it won't be taken into account.");
				setConfidenceExec(at, (conf*this.confidenceFactor));
			}
		else
			while(it.hasNext())
				this.setConfidenceExec(it.next(), this.confidenceFactor);
	}
	
	//Trabajo Futuro: Una idea pendiente de hace tiempo, es la de filtrar las reglas que se pueden disparar, segun un minimo de confianza de las premisas.
	private void executionOneStep(Collection<Rule> rules, Hashtable<String, Double> facts){
		Iterator<Rule> it=rules.iterator(); 
		Rule r;
		while (it.hasNext()) {
			r = it.next();
			if(r.satisfyPremise(facts.keySet())) {
				//System.err.println(r.toString());
				Iterator<String> its=r.getPremise().iterator();
				double confPremise=1.0;
				
//				if(r.getConclusion().contains("TARGET_IsPopulated"))
//					System.out.println(r.toString());
				
				while(its.hasNext()){
					confPremise=Math.min(confPremise,facts.get(its.next()));
				}
				double newConf=r.getConfidence()*confPremise;
				its=r.getConclusion().iterator();
				String c;
				while(its.hasNext()){
					c=its.next();
					if(!this.facts.contains(c)){
						//NOTA: no se refuerza la confianza para los hechos.
						//Actualizamos el valor antiguo con el nuevo. Si no hay valor anterior, this.getConfidenceExec(c)=0.
						double finalConf=this.getConfidenceExec(c)+(newConf*(1.0-this.getConfidenceExec(c)));
						setConfidenceExec(c, finalConf);
					}
				}
				it.remove();
			}
		}
	}

	private void executionAllStep(){
		if(this.facts.isEmpty())
			System.out.println("PS WARNING: No facts have been added to the Production System.");
		this.initialize();
		LinkedList<Rule> rules=this.getRules();
		int size;
		do{
			size=this.conclusions.size();
			Hashtable<String,Double> facts=new Hashtable<String,Double>();
			Iterator<Entry<String,Double>> it=this.conclusions.entrySet().iterator();
			while(it.hasNext()){
				Entry<String,Double> en=it.next();
				facts.put(new String(en.getKey()), new Double(en.getValue().doubleValue()));
			}
			this.executionOneStep(rules,facts);
		}while (this.conclusions.size()!=size);
		
		this.setModified(false);
	}
	
	/**
	 * Return the current confidence threshold for firing rules.
	 * @return the minConfidence
	 */
	public double getMinConfidence() {
		return minConfidence;
	}

	/**
	 * Sets the current confidence threshold for firing rules.
	 * @param minConfidence the minConfidence to set
	 */
	public void setMinConfidence(double minConfidence) {
		if(minConfidence>=0&&minConfidence<=1){
			this.setModified(true);
			this.minConfidence = minConfidence;
		}else{
			System.out.println("PS ERROR: invalid value for Confidence Threshold [0-1]: "+minConfidence);
		}
	}

	/**
	 * Return the current support threshold for firing rules.
	 * @return the minSupport
	 */
	public int getMinSupport() {
		return minSupport;
	}

	/**
	 * Sets the current support threshold for firing rules.
	 * @param minSupport the minSupport to set
	 */
	public void setMinSupport(int minSupport) {
		if(minSupport>=0){
			this.setModified(true);
			this.minSupport = minSupport;
		}else
			System.out.println("PS ERROR: invalid value for Support Threshold (only values>=0): "+minSupport);
	}

	public double getConfidenceFactor() {
		return confidenceFactor;
	}

	public void setConfidenceFactor(double confidenceFactor) {
		this.confidenceFactor = confidenceFactor;
	}

	//Se comprueba si el contexto ha sido modificado y se cambia el valor de modified.
	private boolean isModified() {
		if(context!=null&&this.context.isModified(Context.AR))
			this.setModified(true);
		return modified;
	}

	private void setModified(boolean modified) {
		this.modified = modified;
	}
	
	//DEPURACION
	/**
	 * Depuracion
	 * @deprecated
	 */
	public Set<Entry<String,Double>> getConclusions(){
		if(this.isModified())
			this.executionAllStep();
		return this.conclusions.entrySet();
	}
	//FIN DEPURACION

	/**
	 * @return the initialConfidence
	 */
	public boolean isInitialConfidence() {
		return initialConfidence;
	}

	/**
	 * @param initialConfidence the initialConfidence to set
	 */
	public void setInitialConfidence(boolean initialConfidence) {
		this.initialConfidence = initialConfidence;
	}
	
	public ArrayList<String> getBestConclusions(){
		return this.getBestConclusions(this.getConclusionNames());
	}
	
	public ArrayList<String> getBestEntailedConclusions(LinkedList<String> allTargets){
		LinkedList<String> entailment= new LinkedList<String>();
		LinkedList<String> allConc = this.getConclusionNames();
		for(String target : allTargets) 
			if (allConc.contains(target)) 
				entailment.add(target);
		return this.getBestConclusions(entailment);
	}
	
	public ArrayList<String> getBestConclusions(LinkedList<String> allTargets){
		ArrayList<String> ret=new ArrayList<String>();
		double maxConf=-1;
		Iterator<String> it=allTargets.iterator();
		while(it.hasNext()){
			String att=it.next();
			double conf=this.getConfidence(att);
			if(conf>maxConf){
				ret.clear();
				ret.add(att);
				maxConf=conf;
			}else if(conf==maxConf){
				ret.add(att);
			}
		}
		return ret;
	}
	
	public String toString(){
		String ret="minSupport: "+this.minSupport+" minConf: "+this.minConfidence+"\n";
		ret+=this.getRules().toString();
		return ret;
	}

	public void cleanRules() {
	}
}
