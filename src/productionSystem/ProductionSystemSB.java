/**
 * 
 */
package productionSystem;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;

import context.Context;
import context.Rule;

/**
 * @author Juan Gal&aacute;n P&aacute;ez
 *
 */
public class ProductionSystemSB implements IProductionSystem {
	private LinkedList<Rule> rules;
	private double minConfidence;
	private int minSupport;
	private LinkedHashSet<String> conclusions;
	private LinkedHashSet<String> facts;
	private boolean modified;
	
	public ProductionSystemSB(LinkedList<Rule> _rules, double _minConfidence, int _minSupport){
		this.setRules(_rules);
		this.setMinConfidence(_minConfidence);
		this.setMinSupport(_minSupport);
		this.conclusions=new LinkedHashSet<String>();
		this.facts=new LinkedHashSet<String>();
	}
	
	public ProductionSystemSB(LinkedList<Rule> rules){
		this(rules,1.0,1);
	}
	
	//No mantiene la base de reglas actualizada respecto del contexto.
	public ProductionSystemSB(Context _context, double _minConfidence, int _minSupport){
		this(_context.getRules(),_minConfidence,_minSupport);
		
	}

	public ProductionSystemSB(Context _context){
		this(_context,1.0,1);
	}
	
	public LinkedList<Rule> getRules(){
		return this.rules;
	}
	
	public LinkedList<Rule> getRules(double mincConf, int minSupp){
		LinkedList<Rule> ret= new LinkedList<Rule> ();
		Iterator<Rule> it=this.getRules().iterator();
		while(it.hasNext()){
			Rule r=it.next();
			if(r.getSupport()>=this.minSupport&&r.getConfidence()>=this.getMinConfidence()) {
				ret.add(r);
			}
		}
		return ret;
	}
	
	/**
	 * Return the facts loaded in the Production System. Those will be the initial facts
	 * in the inference process.
	 * @return the facts
	 */
	public LinkedHashSet<String> getFacts() {
		return facts;
	}
	
	/**
	 * Sets the initial facts for the inference process.
	 * @param attributes The facts collection
	 */
	public void setFacts(Collection<String> attributes) {
		this.setModified(true);
		this.facts.clear();
		this.facts.addAll(attributes);
	}
	
	public boolean isConclusion(String c){
		if(this.getConclusions().contains(c))
			return true;
		else{
			if(this.getFacts().contains(c))
				System.out.println("PS WARNING: The conclusion ("+c+") you are asking for is false, because, it is within the initial facts.");
			return false;
		}
	}
	
	/**
	 * return The names of all the conclusions inferred.
	 * @return The names of all the conclusions inferred.
	 */
	public LinkedList<String> getConclusionNames(){
		LinkedList<String> ret= new LinkedList<String>();
		ret.addAll(this.getConclusions());
		return ret;
	}
	
	/**
	 * return Whether the given 'attributes' collection satisfies or not the rule basis.
	 * @param attributes
	 * @return Whether the given 'attributes' collection satisfies or not the rule basis.
	 */
	public boolean satisfyProductionSystem(Collection<String> attributes) {
		Iterator<Rule> it=this.getRules(this.getMinConfidence(),this.getMinSupport()).iterator(); 
		Rule r;
		while (it.hasNext()) {
			r = it.next();
				if(!r.satisfyRule(attributes))
					return false;
		}
		return true;
	}
	
	private Collection<String> executionOneStep(Collection<String> attributes,LinkedList<Rule> lr){
		LinkedList<String> ret=new LinkedList<String>();
		Iterator<Rule> it=lr.iterator(); 
		Rule r;
		while (it.hasNext()) {
			r = it.next();
			if(r.satisfyPremise(attributes)){
				ret.addAll(r.getConclusion());
			}
		}
		return ret;
	}
	
	private void executionAllStep(){
		if(this.facts.isEmpty())
			System.out.println("PS WARNING: No facts have been added to the Production System.");
		this.conclusions.clear();
		LinkedList<Rule> lr=this.getRules(this.getMinConfidence(),this.getMinSupport());
		this.conclusions.addAll(this.getFacts());
		int size;
		do{
			size=this.conclusions.size();
			this.conclusions.addAll(this.executionOneStep(this.conclusions,lr));
			
		}while (this.conclusions.size()!=size);
		this.conclusions.removeAll(this.getFacts());
		this.setModified(false);
	}
	
	/**
	 * @return the minConfidence
	 */
	public double getMinConfidence() {
		return minConfidence;
	}

	/**
	 * Sets the current confidence threshold for firing rules.
	 * @param minConfidence the minConfidence to set
	 */
	public void setMinConfidence(double minConfidence) {
		if(minConfidence>=0&&minConfidence<=1){
			this.setModified(true);
			this.minConfidence = minConfidence;
		}else{
			System.out.println("PS ERROR: invalid value for Confidence Threshold [0-1]: "+minConfidence);
		}
	}

	/**
	 * @return the minSupport
	 */
	public int getMinSupport() {
		return minSupport;
	}

	/**
	 * Sets the current support threshold for firing rules.
	 * @param minSupport the minSupport to set
	 */
	public void setMinSupport(int minSupport) {
		if(minSupport>=0){
			this.setModified(true);
			this.minSupport = minSupport;
		}else
			System.out.println("PS ERROR: invalid value for Support Threshold (only values>=0): "+minSupport);
	}
	
	/**
	 * @param rules the rules to set
	 */
	public void setRules(LinkedList<Rule> rules) {
		this.setModified(true);
		this.rules = rules;
	}

	/**
	 * @return the modified
	 */
	private boolean isModified() {
		return modified;
	}

	/**
	 * @param modified the modified to set
	 */
	private void setModified(boolean modified) {
		this.modified = modified;
	}

	/**
	 * @return the conclusions
	 */
	private LinkedHashSet<String> getConclusions() {
		if(this.isModified())
			this.executionAllStep();
		return conclusions;
	}

	public String toString(){
		String ret="minSupport: "+this.minSupport+" minConf: "+this.minConfidence+"\n";
		ret+=this.getRules().toString();
		return ret;
	}
	
}
