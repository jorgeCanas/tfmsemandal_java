/**
 * 
 */
package productionSystem;

import java.util.Collection;
import java.util.LinkedList;

import context.Rule;

/**
 * @author Juan Gal&aacute;n P&aacute;ez
 *
 */
public interface IProductionSystem {
	public LinkedList<Rule> getRules();
	public double getMinConfidence();
	public int getMinSupport();
	public Collection<String> getFacts();
	public void setMinConfidence(double minConfidence);
	public void setMinSupport(int minSupport);
	public void setFacts(Collection<String> attributes);
	public boolean satisfyProductionSystem(Collection<String> attributes);
	public Collection<String> getConclusionNames();//Metodo comun para ejecucion del PS y consulta de resultados.
	public String toString();
}
