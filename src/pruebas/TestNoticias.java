package pruebas;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;

import DBUtils.MySQLRuleDAO;
import context.Rule;
import motor.SemanticMotor;
import motor.SemanticMotor.CategoryResult;

public class TestNoticias {
	public static void main(String[] args) {
		MySQLRuleDAO mysql = new MySQLRuleDAO();
		mysql.open();
		LinkedList<Rule> myRules = mysql.getRules();
		mysql.close();
		//System.out.println("Rules from database");
		//System.out.println(myRules);
		SemanticMotor sm = new SemanticMotor(myRules);
		// Hechos iniciales
		LinkedList<String> facts=new LinkedList<String>();
		BufferedReader br = null;
		String line = "";
		String word_line = "";
		String firstSplit = ";(?=([^\"]*\"[^\"]*\")*[^\"]*$)";
		String secondSplit = ",(?=([^\"]*\"[^\"]*\")*[^\"]*$)";
		//String factsFile = "ficherosTest/words_category_notices19092016.csv";
		//String factsFile = "ficherosTest/rand_words_category_notices19092016.csv";
		String factsFile = "ficherosTest/rand_words_category_notices21092016.csv";
		try{
			br = new BufferedReader(new InputStreamReader(
		            new FileInputStream(factsFile), "UTF-8"));
			int correct_matchs = 0;
			int total_notices = 0;
			while((line = br.readLine()) != null){
				total_notices++;
				boolean match = false;
				String category_match = "";
				String[] elements = line.split(firstSplit);
				String[] categories = elements[1].split(secondSplit);
				LinkedList<String> listCategories = new LinkedList<String>();
				for(String category: categories){
					listCategories.add(category);
				}
				String[] words = elements[2].split(secondSplit);
				facts = new LinkedList<String>();
				for(String word: words){
					facts.add(word);
				}
				LinkedList<CategoryResult> result = sm.getConclusionNames(facts);
				
				for(CategoryResult cr: result){
					//System.out.print(cr.toString());
					for(String category: listCategories)
						if(category.equals(cr.getCategory())){
							match = true;
							category_match = category;
						}
				}
				if(match){
					System.out.println("Notice match for category " + category_match);
					correct_matchs++;
				}else{
					System.out.println("The category for this notice fails  ");
				}
				
				System.out.println("-------------------------- Next notice facts ---------");
			}
			System.out.println("Hay "+correct_matchs+ " coincidencias de "+total_notices + " posibles");
			float percentage = (float) correct_matchs / (float) total_notices  * 100;
			System.out.println("El porcentage es de un "+ percentage);
			
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}
}
