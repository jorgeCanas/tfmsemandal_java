package pruebas;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

import context.Context;
import context.Rule;
import motor.SemanticMotor;
import motor.SemanticMotor.CategoryResult;
import productionSystem.ProductionSystemAR;
import productionSystem.ProductionSystemSE;
/**
 *  @author Jorge Ca�as Est�vez
 *
 */
public class Prueba5 {
	public static void main (String[] args) {		
		try {
			//Obtenemos el contexto de las noticias de un fichero cxt
			Context cxt = noticias();
			cxt.toConExp("ficherosCXT/noticias2.cxt");
			System.out.println("Fichero cargado y generado");
			//Imprimimos el contexto.
			//System.out.println("CONTEXTO");
			//System.out.println(cxt.toString());
			//Imprimimos todas las reglas.
			System.out.println("\nTODAS LAS REGLAS");
			LinkedList<Rule> misReglas = cxt.getRules(1, 0);
			System.out.println("\nReglas contexto generadas");
			//System.out.println(cxt.getRules(1, 0).toString()+"\n");
			SemanticMotor sm = new SemanticMotor(misReglas);
			//System.out.println(sm.getRules());
			
			// Hechos iniciales para preguntarle al sistema de producción.
			LinkedList<String> facts=new LinkedList<String>();
			BufferedReader br = null;
			String line = "";
			String csvSplitBy = ";(?=([^\"]*\"[^\"]*\")*[^\"]*$)";
			String factsFile = "ficherosCXT/palabras_noticia.csv";
			/*
			try {				
				br = new BufferedReader(new InputStreamReader(
	                    new FileInputStream(factsFile), "UTF-8"));
				
				String[] elements = br.readLine().split(csvSplitBy);
				
				for(String elem: elements){
					facts.add(elem);
				}
				
				br.close();
				System.out.println("Cargado hechos");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			
			LinkedList<CategoryResult> result = sm.getCategories(facts);
			for(CategoryResult cr: result){
				System.out.println(cr.toString());
			}
			*/
			
			try {
				br = new BufferedReader(new InputStreamReader(
	                    new FileInputStream(factsFile), "UTF-8"));
				while((line = br.readLine()) != null){
					String[] elements = line.split(csvSplitBy);
					facts = new LinkedList<String>();
					for(String elem: elements){
						facts.add(elem);
					}
					LinkedList<CategoryResult> result = sm.getConclusionNames(facts);
					for(CategoryResult cr: result){
						System.out.print(cr.toString());
					}
					
					System.out.println("-------------------------- Next notice facts ---------");
				}
				
				br.close();
				System.out.println("Cargado hechos");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
	public static Context noticias(){
		Context cxt = new Context("noticias", true);
		//Abrir archivo 
		String csvFile = "./ficherosCXT/contexto4.csv";
		BufferedReader br = null;
		String line = "";
		String csvSplitBy = ";(?=([^\"]*\"[^\"]*\")*[^\"]*$)";

		try {
			br = new BufferedReader(new InputStreamReader(
                    new FileInputStream(csvFile), "UTF-8"));
			String header = br.readLine();
			String[] attributes = header.split(csvSplitBy);
			for(String attr: attributes){
				cxt.addAttribute(attr);
			}
			while ((line = br.readLine()) != null) {
				String[] data = line.split(csvSplitBy);
				List<String> listaAT = new LinkedList<String>();
				for(int i = 1; i < data.length; i++){
					listaAT.add(data[i]);
				}
				cxt.addObject(data[0], listaAT);
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return cxt;
	}
}