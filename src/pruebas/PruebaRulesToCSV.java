package pruebas;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import context.Context;
import context.Rule;
import motor.SemanticMotor;
import motor.SemanticMotor.CategoryResult;
import productionSystem.ProductionSystemAR;
import productionSystem.ProductionSystemSE;
import utils.IoFile;
/**
 *  @author Jorge Ca�as Est�vez
 *
 */
public class PruebaRulesToCSV {
	public static void main (String[] args) {
		//Obtenemos el contexto de las noticias de un fichero cxt
		
		Context cxt = noticias();
		try {
			cxt.toConExp("ficherosCXT/noticias2.cxt");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//System.out.println("Fichero cargado y generado");
		//Imprimimos el contexto.
		//System.out.println("CONTEXTO");
		//System.out.println(cxt.toString());
		//Imprimimos todas las reglas.
		System.out.println("\nTODAS LAS REGLAS");
		LinkedList<Rule> misReglas = cxt.getRules(1, 0);
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy-HH-mm");
		Date date = new Date();
		String file = "./ficherosReglas/rules"+sdf.format(date)+".csv";
		IoFile.saveCSVFile(file, misReglas);
		System.out.println(misReglas);
		//String file = "./ficherosReglas/rules08-08-2016-17-00.csv";
		LinkedList<Rule> loadedRules = IoFile.getRulesFromCSV(file);
		//System.out.println(loadedRules);
		//Prueba de regla
		//tutor;transversal;notas;formador;docente
		Rule r = new Rule();
		r.addPremise("tutor");
		r.addPremise("transversal");
		r.addConclusion("notas");
		r.addConclusion("formador");
		r.addConclusion("ETIQUETA_prueba");
		r.setConfidence(1.0);
		r.setSupport(20);
		//misReglas.add(r);
		SemanticMotor sm = new SemanticMotor(misReglas);
		//SemanticMotor sm = new SemanticMotor(loadedRules);
		
		
		// Hechos iniciales
		LinkedList<String> facts=new LinkedList<String>();
		BufferedReader br = null;
		String line = "tutor;transversal;notas;formador;docente";
		String csvSplitBy = ";(?=([^\"]*\"[^\"]*\")*[^\"]*$)";
		//String factsFile = "ficherosCXT/palabras_noticia.csv";
		String factsFile = "ficherosCXT/palabras_noticia09082016.csv";
		
		/*
		String[] elements = line.split(csvSplitBy);
		facts = new LinkedList<String>();
		for(String elem: elements){
			facts.add(elem);
		}
		LinkedList<CategoryResult> result = sm.getConclusionNames(facts);
		for(CategoryResult cr: result){
			System.out.print(cr.toString());
		}
		*/
		try{
			br = new BufferedReader(new InputStreamReader(
		            new FileInputStream(factsFile), "UTF-8"));
			while((line = br.readLine()) != null){
				String[] elements = line.split(csvSplitBy);
				facts = new LinkedList<String>();
				for(String elem: elements){
					facts.add(elem);
				}
				LinkedList<CategoryResult> result = sm.getConclusionNames(facts);
				
				for(CategoryResult cr: result){
					System.out.print(cr.toString());
				}
				
				System.out.println("-------------------------- Next notice facts ---------");
			}
			
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
	public static Context noticias(){
		Context cxt = new Context("noticias", true);
		//Abrir archivo 
		//String csvFile = "./ficherosCXT/contexto06082016.csv";
		//String csvFile = "./ficherosCXT/contexto06082016-8.csv";
		String csvFile = "./ficherosCXT/contexto08082016-400-18.csv";
		//String csvFile = "./ficherosCXT/contexto4.csv";
		BufferedReader br = null;
		String line = "";
		String csvSplitBy = ";(?=([^\"]*\"[^\"]*\")*[^\"]*$)";

		try {
			br = new BufferedReader(new InputStreamReader(
                    new FileInputStream(csvFile), "UTF-8"));
			String header = br.readLine();
			String[] attributes = header.split(csvSplitBy);
			for(String attr: attributes){
				cxt.addAttribute(attr);
			}
			while ((line = br.readLine()) != null) {
				String[] data = line.split(csvSplitBy);
				List<String> listaAT = new LinkedList<String>();
				for(int i = 1; i < data.length; i++){
					listaAT.add(data[i]);
				}
				cxt.addObject(data[0], listaAT);
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return cxt;
	}
}