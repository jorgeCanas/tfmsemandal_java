package pruebas;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

import context.Context;
import context.Rule;
import motor.SemanticMotor;
import motor.SemanticMotor.CategoryResult;
import productionSystem.ProductionSystemAR;
import productionSystem.ProductionSystemSE;
/**
 *  @author Jorge Ca�as Est�vez
 *
 */
public class PruebaBasica2 {
	public static void main (String[] args) {		
		//Imprimimos todas las reglas.
		System.out.println("\nTODAS LAS REGLAS");
		LinkedList<Rule> misReglas = new LinkedList<Rule>();
		
		//Prueba de regla
		//tutor;transversal;notas;formador;docente
		Rule r = new Rule();
		r.addPremise("tutor");
		r.addPremise("transversal");
		r.addConclusion("notas");
		r.addConclusion("formador");
		r.addConclusion("ETIQUETA_prueba");
		r.setConfidence(1.0);
		r.setSupport(20);
		misReglas.add(r);
		
		System.out.println(misReglas);
		SemanticMotor sm = new SemanticMotor(misReglas);
		
		
		// Hechos iniciales
		LinkedList<String> facts=new LinkedList<String>();
		BufferedReader br = null;
		String line = "tutor;transversal;notas;formador;docente";
		String csvSplitBy = ";(?=([^\"]*\"[^\"]*\")*[^\"]*$)";
		String factsFile = "ficherosCXT/palabras_noticia.csv";
		
		/*
		String[] elements = line.split(csvSplitBy);
		facts = new LinkedList<String>();
		for(String elem: elements){
			facts.add(elem);
		}
		LinkedList<CategoryResult> result = sm.getConclusionNames(facts);
		for(CategoryResult cr: result){
			System.out.print(cr.toString());
		}
		*/
		try {
			br = new BufferedReader(new InputStreamReader(
		            new FileInputStream(factsFile), "UTF-8"));
			while((line = br.readLine()) != null){
				String[] elements = line.split(csvSplitBy);
				facts = new LinkedList<String>();
				for(String elem: elements){
					facts.add(elem);
				}
				LinkedList<CategoryResult> result = sm.getConclusionNames(facts);
				for(CategoryResult cr: result){
					System.out.print(cr.toString());
				}
				
				System.out.println("-------------------------- Next notice facts ---------");
			}
			
			br.close();
			System.out.println("Cargado hechos");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
	public static Context noticias(){
		Context cxt = new Context("noticias", true);
		//Abrir archivo 
		//String csvFile = "./ficherosCXT/contexto06082016.csv";
		String csvFile = "./ficherosCXT/contexto06082016-8.csv";
		//String csvFile = "./ficherosCXT/contexto4.csv";
		BufferedReader br = null;
		String line = "";
		String csvSplitBy = ";(?=([^\"]*\"[^\"]*\")*[^\"]*$)";

		try {
			br = new BufferedReader(new InputStreamReader(
                    new FileInputStream(csvFile), "UTF-8"));
			String header = br.readLine();
			String[] attributes = header.split(csvSplitBy);
			for(String attr: attributes){
				cxt.addAttribute(attr);
			}
			while ((line = br.readLine()) != null) {
				String[] data = line.split(csvSplitBy);
				List<String> listaAT = new LinkedList<String>();
				for(int i = 1; i < data.length; i++){
					listaAT.add(data[i]);
				}
				cxt.addObject(data[0], listaAT);
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return cxt;
	}
}