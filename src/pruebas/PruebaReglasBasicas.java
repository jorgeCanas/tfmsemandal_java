package pruebas;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;

import context.Rule;
import motor.SemanticMotor;
import motor.SemanticMotor.CategoryResult;

public class PruebaReglasBasicas {

	public static void main(String[] args) {
		LinkedList<Rule> myRules;
		myRules = setBasicRules();
		SemanticMotor sm = new SemanticMotor(myRules);
		
		// Hechos iniciales
		LinkedList<String> facts=new LinkedList<String>();
		String csvSplitBy = ";(?=([^\"]*\"[^\"]*\")*[^\"]*$)";
		String factsFile = "ficherosCXT/palabras_noticia_basica.csv";

		BufferedReader br = null;
		String line = "";
		try {
			br = new BufferedReader(new InputStreamReader(
                    new FileInputStream(factsFile), "UTF-8"));
			while((line = br.readLine()) != null){
				String[] elements = line.split(csvSplitBy);
				facts = new LinkedList<String>();
				for(String elem: elements){
					facts.add(elem);
				}
				LinkedList<CategoryResult> result = sm.getConclusionNames(facts);
				for(CategoryResult cr: result){
					System.out.print(cr.toString());
				}
				
				System.out.println("-------------------------- Next notice facts ---------");
			}
			
			br.close();
			System.out.println("Cargado hechos");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	private static LinkedList<Rule> setBasicRules() {
		//ayuntamiento;jornadas;centro
		//juventud;ayuntamiento;centro;plaza;docente
		//acta;plaza;gestion;tecnico;calificacion
		LinkedList<Rule> myRules = new LinkedList<Rule>();
		Rule r = new Rule();
		//Rule 1 ayuntamiento --(100%)--> ETIQUETA_general
		r.addPremise("ayuntamiento");
		r.addConclusion("ETIQUETA_general");
		r.setConfidence(1.0);
		r.setSupport(20);
		myRules.add(r);
		//Rule 2 centro, plaza --(100%)--> docente, ETIQUETA_educacion, ETIQUETA_cursos
		r = new Rule();
		r.addPremise("centro");
		r.addPremise("plaza");
		r.addConclusion("docente");
		r.addConclusion("ETIQUETA_educacion");
		r.addConclusion("ETIQUETA_cursos");
		r.setConfidence(1.0);
		r.setSupport(10);
		myRules.add(r);
		//Rule 3 acta, gestion, tecnico --(80%)--> calificacion, ETIQUETA_empleo
		r = new Rule();
		r.addPremise("acta");
		r.addPremise("gestion");
		r.addPremise("tecnico");
		r.addConclusion("calificacion");
		r.addConclusion("ETIQUETA_empleo");
		r.setConfidence(0.8);
		r.setSupport(4);
		myRules.add(r);
		return myRules;
	}
}
