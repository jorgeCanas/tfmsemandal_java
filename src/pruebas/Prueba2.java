package pruebas;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

import productionSystem.ProductionSystemAR;

import context.Context;
import context.Rule;
import motor.SemanticMotor;
import motor.SemanticMotor.CategoryResult;
/**
 *  @author Jorge Ca�as Est�vez
 *
 */
public class Prueba2 {
	
	public static void main (String[] args) {
		try {
			//Obtenemos el contexto de las noticias de un fichero cxt
			Context cxt = noticias();
			cxt.toConExp("ficherosCXT/noticias2.cxt");
			System.out.println("Fichero cargado y generado");
			//Imprimimos el contexto.
			//System.out.println("CONTEXTO");
			//System.out.println(cxt.toString());
			//Imprimimos todas las reglas.
			System.out.println("\nTODAS LAS REGLAS");
			LinkedList<Rule> misReglas = cxt.getRules(1, 0);
			System.out.println("\nReglas contexto generadas");
			//System.out.println(cxt.getRules(1, 0).toString()+"\n");
			SemanticMotor sm = new SemanticMotor(misReglas);
			//System.out.println(sm.getRules());
			
			// Hechos iniciales para preguntarle al sistema de producción.
			LinkedList<String> facts=new LinkedList<String>();
			BufferedReader br = null;
			String line = "";
			String csvSplitBy = ";(?=([^\"]*\"[^\"]*\")*[^\"]*$)";
			String factsFile = "ficherosCXT/palabras_noticia.csv";
			/*
			try {				
				br = new BufferedReader(new InputStreamReader(
	                    new FileInputStream(factsFile), "UTF-8"));
				
				String[] elements = br.readLine().split(csvSplitBy);
				
				for(String elem: elements){
					facts.add(elem);
				}
				
				br.close();
				System.out.println("Cargado hechos");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			
			LinkedList<CategoryResult> result = sm.getCategories(facts);
			for(CategoryResult cr: result){
				System.out.println(cr.toString());
			}
			*/
			
			try {
				br = new BufferedReader(new InputStreamReader(
	                    new FileInputStream(factsFile), "UTF-8"));
				while((line = br.readLine()) != null){
					String[] elements = line.split(csvSplitBy);
					facts = new LinkedList<String>();
					for(String elem: elements){
						facts.add(elem);
					}
					LinkedList<CategoryResult> result = sm.getConclusionNames(facts);
					for(CategoryResult cr: result){
						System.out.print(cr.toString());
					}
					
					System.out.println("-------------------------- Next notice facts ---------");
				}
				
				br.close();
				System.out.println("Cargado hechos");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			
			
			// Inicilizamos el sistema de producción:
			/*
			 * Parámetros:
			 * 1) Context cxt: Es el contexto que previamente hemos creado y es el que el sistema usará para
			 * calcular las reglas necesarias para el razonamiento.
			 * 2) double minConfidence: valores entre 0 y 1.0. Sirve para seleccionar el conjunto de reglas a usar
			 * en el razonamiento. Si ponemos 1, tomamos solo la base Stem. Se usaran
			 * en el razonamiento todas las reglas con confianza >= minConfidence.
			 * 3) int minSupport: Normalmente lo dejamos a 1. Funciona igual que el parámetro anterior. Seleccionamos
			 * todas las reglas con soporte >= minSupport.
			 * 4) boolean initialConfidente: De momento podemos dejarlo a false. Si está a true la confianza inicial
			 * de los hechos se calculará según la frecuencia relativa de estos en el contexto.
			 * 5) double confidenceFactor: por defecto lo dejamos a 1. A veces pasa (como pasó en el ajedrez) que las confianzas deducidas por
			 * el sistema de producción son todas 1.0 o 0.999999..., de forma que no podemos decidir cual de las
			 * conclusiones obtenidas es la mejor. Este parámetro nos permitirá controlar el problema. Usando un confidenceFactor < 1 (y siempre > 0)
			 * reducimos el refuerzo de las confianzas de las conclusiones. En el caso del ajedrez como había tanto refuerzo yo usé un confidenceFactor=0.005.
			 * El factor necesario dependerá mucho del conjunto de atributos y objetos usado.
			 */
			ProductionSystemAR ps=new ProductionSystemAR(cxt,0.7,1,false,1);
			ps.setFacts(facts);
			
			System.out.println("\nSISTEMA DE PRODUCCION");
			//System.out.println(ps.toString());
			// ps.toString imprime los parámetros del sistema de producción y el subconjunto de reglas que se ha usado en el razonamiento.
			
			System.out.println("\nRAZONAMIENTO");
			System.out.println("Hechos: "+facts);
			System.out.println("Resultado: "+ps.getConclusionNames());
			
			//System.out.println("Oceano: "+ps.getConfidence("Oceano"));
			//System.out.println("Litoral: "+ps.getConfidence("Litoral"));
			//System.out.println("Fluvial: "+ps.getConfidence("Fluvial"));
			
			/*
			 * ps.getConclusionNames() nos devuelve las confianzas de las conclusiones deducidas, que  son
			 * las únicas que nos interesan. El método ps.getConfidence("nombreAtributo") nos permite
			 * preguntar la confianza de un atributo cualquiera (aunque no haya sido deducido). Si no ha sido
			 * deducido devolverá 0, y si el atributo era un hecho inicial, devolverá una confianza que no nos
			 * interesa ya que al ser un hecho inicial, sabemos a priori que es cierto!!
			 */
			

		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
	public static Context noticias(){
		Context cxt = new Context("noticias", true);
		//Abrir archivo 
		//String csvFile = "./ficherosCXT/contexto.csv";
		String csvFile = "./ficherosCXT/contexto2.csv";
		BufferedReader br = null;
		String line = "";
		String csvSplitBy = ";(?=([^\"]*\"[^\"]*\")*[^\"]*$)";

		try {
			br = new BufferedReader(new InputStreamReader(
                    new FileInputStream(csvFile), "UTF-8"));
			String header = br.readLine();
			String[] attributes = header.split(csvSplitBy);
			for(String attr: attributes){
				cxt.addAttribute(attr);
			}
			while ((line = br.readLine()) != null) {
				String[] data = line.split(csvSplitBy);
				List<String> listaAT = new LinkedList<String>();
				for(int i = 1; i < data.length; i++){
					listaAT.add(data[i]);
				}
				cxt.addObject(data[0], listaAT);
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return cxt;
	}
}
