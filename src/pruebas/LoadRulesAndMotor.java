package pruebas;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import DBUtils.MySQLRuleDAO;
import context.Rule;
import motor.SemanticMotor;
import motor.SemanticMotor.CategoryResult;

public class LoadRulesAndMotor {

	public static void main(String[] args) {
		MySQLRuleDAO mysql = new MySQLRuleDAO();
		mysql.open();
		LinkedList<Rule> myRules = mysql.getRules();
		mysql.close();
		System.out.println("Rules from database");
		System.out.println(myRules);
		SemanticMotor sm = new SemanticMotor(myRules);
		// Hechos iniciales
		LinkedList<String> facts=new LinkedList<String>();
		BufferedReader br = null;
		String line = "";
		String csvSplitBy = ";(?=([^\"]*\"[^\"]*\")*[^\"]*$)";
		//String factsFile = "ficherosCXT/palabras_noticia.csv";
		String factsFile = "ficherosCXT/rand_words_category_notices200.csv";
		try{
			br = new BufferedReader(new InputStreamReader(
		            new FileInputStream(factsFile), "UTF-8"));
			int nlines = 0;
			int nHasSomething = 0;
			while((line = br.readLine()) != null){
				nlines++;
				List<String> elements = new ArrayList<String>(Arrays.asList(line.split(csvSplitBy)));
				elements.remove(0);
				facts = new LinkedList<String>();
				for(String elem: elements){
					facts.add(elem);
				}
				LinkedList<CategoryResult> result = sm.getConclusionNames(facts);
				if (result.size() > 0)
					nHasSomething++;
				for(CategoryResult cr: result){
					System.out.print(cr.toString());
				}
				
				System.out.println("-------------------------- Next notice facts ---------");
			}
			System.out.println("Numero de lineas/noticas " 
			+ nlines + " con al menos 1 coincidencia " + nHasSomething);
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

}
