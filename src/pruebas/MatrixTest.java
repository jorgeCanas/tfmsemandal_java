package pruebas;

import java.util.ArrayList;
import java.util.List;

import motor.ConfussionMatrix;

public class MatrixTest {

	private static final int List = 0;

	public static void main(String[] args) {
		String[] labels = {"generales", "social", "juventud", "empleo", "cultura" };
		//ConfussionMatrix matrix = new ConfussionMatrix(labels);
		ConfussionMatrix matrix = new ConfussionMatrix();
		List<String> assignValues = new ArrayList<String>();
		assignValues.add("generales");
		assignValues.add("social");
		List<String> givenValues = new ArrayList<String>();
		givenValues.add("social");
		givenValues.add("juventud");
		givenValues.add("empleo");
		givenValues.add("cultura");
		matrix.setValues(assignValues, givenValues);
		//matrix.setValues(givenValues, assignValues);
		/*
		matrix.setValue("generales", "social");
		//System.out.println(matrix.getMatrix());
		matrix.setValue("generales", "juventud");
		//System.out.println(matrix.getMatrix());
		matrix.setValue("generales", " empleo");
		//System.out.println(matrix.getMatrix());
		matrix.setValue("generales", "cultura");
		matrix.setValue("social", "social");
		//System.out.println(matrix.getMatrix());
		matrix.setValue("social", "juventud");
		matrix.setValue("social", " empleo");
		matrix.setValue("social", "cultura");
		*/
		System.out.println(matrix.getMatrix());
		System.out.println(matrix.getTraza());
		System.out.println(matrix.getErrors());
	}

}
