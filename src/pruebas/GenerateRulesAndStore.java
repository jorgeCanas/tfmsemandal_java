package pruebas;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import DBUtils.MySQLRuleDAO;
import context.Context;
import context.Rule;

public class GenerateRulesAndStore {
	//GenerateRulesAndStore.jar -out ./path/to/file -ctx ./path/to/context -limit 5 -num 50
	//java -jar tika-app.jar -t %s > textOutput.txt' % fileName
	public static void main(String[] args) {
		Map<String, String> values = null;
		
		//Obtenemos el contexto de las noticias de un fichero cxt
		try {
			
			values = argParser(args);
			/*
			for(String key: values.keySet()){
				System.out.println("Key "+ key + " value " + values.get(key));
			}
			
			String contextfile = values.get("-out") + "-" + values.get("-num") + "-" +
					values.get("-limit") + ".csv";
			System.out.println(contextfile);
			*/
			Context cxt = noticias(values.get("-ctx"));
			//System.out.println("Fichero cargado y generado");
			//Imprimimos el contexto.
			//System.out.println("CONTEXTO");
			//System.out.println(cxt.toString());
			//Imprimimos todas las reglas.
			System.out.println("\nTODAS LAS REGLAS CON SOPORTE >= 1");
			LinkedList<Rule> myRules = cxt.getRules(1, 0);
			System.out.println("----------------------Reglas v�lidas "+myRules.size());
			//System.out.println(myRules);
			
			MySQLRuleDAO mysql = new MySQLRuleDAO();
			mysql.open();
			mysql.insert(myRules);
			mysql.close();
			System.out.println("Reglas generadas y almacenadas en la base de datos");
			
		}catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static Context noticias(String csvFile) throws Exception{
		Context cxt = new Context("noticias", true);
		BufferedReader br = null;
		String line = "";
		String csvSplitBy = ";(?=([^\"]*\"[^\"]*\")*[^\"]*$)";

		try {
			br = new BufferedReader(new InputStreamReader(
                    new FileInputStream(csvFile), "UTF-8"));
			String header = br.readLine();
			String[] attributes = header.split(csvSplitBy);
			for(String attr: attributes){
				cxt.addAttribute(attr);
			}
			while ((line = br.readLine()) != null) {
				String[] data = line.split(csvSplitBy);
				List<String> listaAT = new LinkedList<String>();
				for(int i = 1; i < data.length; i++){
					listaAT.add(data[i]);
				}
				cxt.addObject(data[0], listaAT);
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw e;
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return cxt;
	}
	
	public static Map<String, String> argParser(String[] args) throws IllegalArgumentException{
		Map<String, String> argsList = new HashMap<String, String>();
		/*
		System.out.print("Args: ");
		for(String value: args){
			System.out.print(value + " ");
		}
		System.out.println();
		*/
	    for (int i = 0; i < args.length; i++) {
	    	if(args[i].startsWith("-")){
	    		if (args.length-1 == i || args[i + 1].startsWith("-"))
	                throw new IllegalArgumentException("Expected arg after: "+args[i]);
	    		argsList.put(args[i], args[i+1]);
	    	}
	    }
	    if(!argsList.containsKey("-ctx"))
	    	throw new IllegalArgumentException("Context file not set. Check if -ctx is correctly spelled");

	    /*
	    if(argsList.size() < 4){
	    	throw new IllegalArgumentException("Need at least 4 arguments");
	    }
	    
	    List<String> parse = Arrays.asList("-out", "-ctx", "-limit", "-num");
	    if(!parse.containsAll(argsList.keySet()))
	    	throw new IllegalArgumentException("Wrong arguments, check that everything is OK.");
	    */
	    return argsList;
	}

}


