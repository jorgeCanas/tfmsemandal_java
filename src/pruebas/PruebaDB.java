package pruebas;

import java.util.LinkedList;

import DBUtils.MySQLRuleDAO;
import context.Rule;

public class PruebaDB {

	public static void main(String[] args) {
		MySQLRuleDAO mysql = new MySQLRuleDAO();
		mysql.open();
		LinkedList<Rule> rules = new LinkedList<Rule>();
		Rule r = new Rule();
		r.setConfidence(1.0);
		r.setSupport(15);
		r.addPremise("alcalde");
		r.addPremise("patrimonio");
		r.addConclusion("historico");
		r.addConclusion("ETIQUETA_patrimonio");
		rules.add(r);
		mysql.insert(rules);
		rules = mysql.getRules();
		for(Rule rule: rules){
			System.out.println(rule.toString());
		}
		mysql.close();
	}

}
