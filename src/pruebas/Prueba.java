package pruebas;

import java.io.IOException;

import java.util.LinkedList;
import java.util.List;
import productionSystem.ProductionSystemAR;
import context.Context;
import context.Rule;

/**
 *  @author Juan Gal&aacute;n P&aacute;ez
 *
 */
public class Prueba {
	
	public static void main (String[] args) {		
		try {
			// Obtenemos el contexto de los peces.
			Context cxt=peces();
			// Con este método podemos generar ficheros .cxt (formato ConExp).
			cxt.toConExp("ficherosCXT/peces.cxt");
			//Imprimimos el contexto.
			System.out.println("CONTEXTO");
			System.out.println(cxt.toString());
			//Imprimimos todas las reglas.
			System.out.println("\nTODAS LAS REGLAS");
			//generar list de getRules
			LinkedList<Rule> misReglas = cxt.getRules(0, 0);
			
			System.out.println(cxt.getRules(0, 0).toString()+"\n");
			
			// Hechos iniciales para preguntarle al sistema de producción.
			LinkedList<String> facts=new LinkedList<String>();
			facts.add("Litoral");
//			facts.add("Oceano");
//			facts.add("Fluvial");
			
			
			// Inicilizamos el sistema de producción:
			/*
			 * Parámetros:
			 * 1) Context cxt: Es el contexto que previamente hemos creado y es el que el sistema usará para
			 * calcular las reglas necesarias para el razonamiento.
			 * 2) double minConfidence: valores entre 0 y 1.0. Sirve para seleccionar el conjunto de reglas a usar
			 * en el razonamiento. Si ponemos 1, tomamos solo la base Stem. Se usaran
			 * en el razonamiento todas las reglas con confianza >= minConfidence.
			 * 3) int minSupport: Normalmente lo dejamos a 1. Funciona igual que el parámetro anterior. Seleccionamos
			 * todas las reglas con soporte >= minSupport.
			 * 4) boolean initialConfidente: De momento podemos dejarlo a false. Si está a true la confianza inicial
			 * de los hechos se calculará según la frecuencia relativa de estos en el contexto.
			 * 5) double confidenceFactor: por defecto lo dejamos a 1. A veces pasa (como pasó en el ajedrez) que las confianzas deducidas por
			 * el sistema de producción son todas 1.0 o 0.999999..., de forma que no podemos decidir cual de las
			 * conclusiones obtenidas es la mejor. Este parámetro nos permitirá controlar el problema. Usando un confidenceFactor < 1 (y siempre > 0)
			 * reducimos el refuerzo de las confianzas de las conclusiones. En el caso del ajedrez como había tanto refuerzo yo usé un confidenceFactor=0.005.
			 * El factor necesario dependerá mucho del conjunto de atributos y objetos usado.
			 */
			
			ProductionSystemAR ps=new ProductionSystemAR(cxt,0.7,1,false,1);
			ps.setFacts(facts);
			
			
			System.out.println("\nSISTEMA DE PRODUCCION");
			System.out.println(ps.toString());
			// ps.toString imprime los parámetros del sistema de producción y el subconjunto de reglas que se ha usado en el razonamiento.
			
			
			System.out.println("\nRAZONAMIENTO");
			System.out.println("Hechos: "+facts);
			System.out.println("Resultado: "+ps.getConclusionNames());
			
			System.out.println("Oceano: "+ps.getConfidence("Oceano"));
			System.out.println("Litoral: "+ps.getConfidence("Litoral"));
			System.out.println("Fluvial: "+ps.getConfidence("Fluvial"));
			
			/*
			 * ps.getConclusionNames() nos devuelve las confianzas de las conclusiones deducidas, que  son
			 * las únicas que nos interesan. El método ps.getConfidence("nombreAtributo") nos permite
			 * preguntar la confianza de un atributo cualquiera (aunque no haya sido deducido). Si no ha sido
			 * deducido devolverá 0, y si el atributo era un hecho inicial, devolverá una confianza que no nos
			 * interesa ya que al ser un hecho inicial, sabemos a priori que es cierto!!
			 */
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
	// Crear el contexto Peces.
	public static Context peces(){
		
		/*
		 * Crear un nuevo contexto. Parámetros:
		 * String Nombre.
		 * Boolean rigid: Normalmente lo dejamos a true. Antes de introducir ningún objeto al contexto, debemos
		 * introducir todos los atributos que vayamos a usar. Si introducimos algún objeto que contiene un atributo,
		 * el cuál no existe en el contexto, se producirá un error. Si ponemos rigid = false este error se ignorará y
		 * el atributo será añadido al contexto.
		 */
		Context cxt=new Context("peces",true);
		
		//Añadimos todos los atributos del contexto.
		cxt.addAttribute("Fluvial");
		cxt.addAttribute("Litoral");
		cxt.addAttribute("Oceano");
		
		//Añadimos los objetos.
		/*
		 * Un objeto está formado por su identificador (String) y el conjunto
		 * de atributos que contiene (Collection<String>). De esta forma el método
		 * addObject recibe el identificador del objeto y una colección con los identificadores de
		 * todos los atributos que contiene.
		 */
		List<String> l =new LinkedList<String>();
		l.add("Fluvial");
		cxt.addObject("Carpa", l);

		l =new LinkedList<String>();
		l.add("Fluvial");
		l.add("Litoral");
		cxt.addObject("Escato", l);
	
		l =new LinkedList<String>();
		l.add("Litoral");
		l.add("Oceano");
		cxt.addObject("Sargo", l);

		l =new LinkedList<String>();
		l.add("Litoral");
		l.add("Oceano");
		cxt.addObject("Dorada", l);

		l =new LinkedList<String>();
		l.add("Fluvial");
		l.add("Litoral");
		l.add("Oceano");
		cxt.addObject("Anguila", l);
		
		return cxt;
	}
	
	
	// Otro contexto de ejemplo.
	public static Context cla2008(){
		Context cxt=new Context("cla2008",true);

		cxt.addAttribute("Acyclic");
		cxt.addAttribute("Connected");
		cxt.addAttribute("2-connected");
		cxt.addAttribute("Geodetic");
		cxt.addAttribute("Bipartite");
		cxt.addAttribute("Nonseparable");
		cxt.addAttribute("Planar");
		cxt.addAttribute("Tree");
		cxt.addAttribute("rad-minimal");
		
		List<String> l =new LinkedList<String>();
		l.add("Acyclic");
		l.add("Connected");
		l.add("Geodetic");
		l.add("Bipartite");
		l.add("Planar");
		l.add("Tree");
		l.add("rad-minimal");
		cxt.addObject("obj1", l);

		l =new LinkedList<String>();
		l.add("Connected");
		l.add("2-connected");
		l.add("Bipartite");
		l.add("Nonseparable");
		l.add("Planar");
		cxt.addObject("obj2", l);
		
		l =new LinkedList<String>();
		l.add("Connected");
		l.add("2-connected");
		l.add("Geodetic");
		l.add("Nonseparable");
		cxt.addObject("obj3", l);

		l =new LinkedList<String>();
		l.add("Acyclic");
		l.add("Bipartite");
		l.add("Planar");
		cxt.addObject("obj4", l);

		l =new LinkedList<String>();
		l.add("Connected");
		l.add("2-connected");
		l.add("Geodetic");
		l.add("Nonseparable");
		l.add("Planar");
		cxt.addObject("obj5", l);

		l =new LinkedList<String>();
		l.add("Connected");
		l.add("2-connected");
		l.add("Bipartite");
		l.add("Nonseparable");
		cxt.addObject("obj6", l);

		l =new LinkedList<String>();
		l.add("Connected");
		l.add("2-connected");
		l.add("Bipartite");
		l.add("Planar");
		cxt.addObject("obj7", l);

		l =new LinkedList<String>();
		l.add("Connected");
		l.add("2-connected");
		l.add("Geodetic");
		l.add("Planar");
		cxt.addObject("obj8", l);

		return cxt;
	}
}
