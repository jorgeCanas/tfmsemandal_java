package DBUtils;

import java.util.LinkedList;
import context.Rule;

public interface RuleDAO {
	public void insert(LinkedList<Rule> rules);
	public LinkedList<Rule> getRules();
}
