package DBUtils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.Properties;

import context.Rule;

public class MySQLRuleDAO implements RuleDAO{
	private Connection con = null;
	
	public void open(){
		Properties prop = new Properties();
		try {
			try{
				prop.load(MySQLRuleDAO.class.getResourceAsStream("/ficherosPropiedades/semandal.properties"));
			}catch(NullPointerException e){
				prop.load(new FileInputStream("ficherosPropiedades/semandal.properties"));
			}
			String user = prop.getProperty("user");
			String password = prop.getProperty("password");
			String dburl = prop.getProperty("dburl");
			Class.forName("com.mysql.jdbc.Driver");
			if(con == null){
				con = DriverManager.getConnection(dburl, user, password);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void close(){
		DbUtil.close(con);
	}

	@Override
	public void insert(LinkedList<Rule> rules) {
		if(con!=null){
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				//this.createTables();
				this.deleteTablesContent();
				for(Rule r: rules){
					//Transaction
					String insRule = "INSERT INTO rule(`support`, `confidence`) VALUES(?,?)";
					con.setAutoCommit(false);
					ps = con.prepareStatement(insRule, Statement.RETURN_GENERATED_KEYS);
					int i = 1;
					ps.setInt(i++, r.getSupport());
					ps.setDouble(i++, r.getConfidence());
					ps.executeUpdate();
					rs = ps.getGeneratedKeys();
					if(rs.next()){
						long r_id = rs.getLong(1);
						for(String p: r.getPremise()){
							String insPremise = "INSERT INTO premise(`p_word`, `r_id`) VALUES(?,?)";
							ps = con.prepareStatement(insPremise);
							i = 1;
							ps.setString(i++, p);
							ps.setLong(i++, r_id);
							ps.executeUpdate();
							//DbUtil.close(ps);
						}
						for(String c: r.getConclusion()){
							String insConclusion = "INSERT INTO conclusion(`c_word`, `r_id`) VALUES(?,?)";
							ps = con.prepareStatement(insConclusion);
							i = 1;
							ps.setString(i++, c);
							ps.setLong(i++, r_id);
							ps.executeUpdate();
							//DbUtil.close(ps);
						}
						con.commit();
					}/*else{
						con.rollback();
						System.out.println("Insert problem with the rule "+ r.toString());
					}*/
				}
			} catch (SQLException e) {
				if(con != null){
					try {
						System.out.println("Problem with the insert, rollback made");
						con.rollback();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}
				e.printStackTrace();
			}finally {
				DbUtil.close(rs);
				DbUtil.close(ps);
				try {
					con.setAutoCommit(true);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}else{
			System.out.println("Need an open connection");
		}
	}
	
	private void createTables(){
		if(con!=null){
			Statement stmt = null;
			try {
				stmt = con.createStatement();
				String createRule = "CREATE TABLE IF NOT EXISTS `rule` (" +
						  "`r_id` bigint(20) NOT NULL PRIMARY KEY AUTO_INCREMENT," +
						  "`support` int(20) DEFAULT '0'," +
						  "`confidence` float DEFAULT '0'" +
						  ") ENGINE=InnoDB DEFAULT CHARSET=utf8";
				
				String createConclusion = "CREATE TABLE IF NOT EXISTS `conclusion` ("+
						  "`c_id` bigint(20) NOT NULL PRIMARY KEY AUTO_INCREMENT," +
						  "`c_word` varchar(70) DEFAULT NULL," +
						  "`r_id` bigint(20) NOT NULL," +
						  "FOREIGN KEY (r_id) REFERENCES rule(r_id)" +
						  ") ENGINE=InnoDB DEFAULT CHARSET=utf8";
				
				String createPremise = "CREATE TABLE IF NOT EXISTS `premise` (" +
						  "`p_id` bigint(20) NOT NULL PRIMARY KEY AUTO_INCREMENT," +
						  "`p_word` varchar(70) DEFAULT NULL," +
						  "`r_id` bigint(20) NOT NULL," +
						  "FOREIGN KEY (r_id) REFERENCES rule(r_id)" +
						  ") ENGINE=InnoDB DEFAULT CHARSET=utf8";
				
				stmt.execute(createRule);
				stmt.execute(createConclusion);
				stmt.execute(createPremise);
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				DbUtil.close(stmt);
			}
		}else{
			System.out.println("Need an open connection");
		}
	}
	
	private void deleteTablesContent(){
		if(con!=null){
			Statement stmt = null;
			try {
				stmt = con.createStatement();
				String queryPremise = "DELETE FROM premise";
				String queryConclusion = "DELETE FROM conclusion";
				String queryRule = "DELETE FROM rule";
				stmt.executeUpdate(queryPremise);
				stmt.executeUpdate(queryConclusion);
				stmt.executeUpdate(queryRule);
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				DbUtil.close(stmt);
			}
		}else{
			System.out.println("Need an open connection");
		}
	}

	@Override
	public LinkedList<Rule> getRules() {
		LinkedList<Rule> rules = new LinkedList<Rule>();
		Statement stmt = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ResultSet rs_p = null;
		ResultSet rs_c = null;
		if(con!=null){
			try {
				stmt = con.createStatement();
				String selectRules = "SELECT * FROM rule";
				rs = stmt.executeQuery(selectRules);
				while(rs.next()){
					Rule r = new Rule();
					long r_id = rs.getLong("r_id");
					r.setSupport(rs.getInt("support"));
					r.setConfidence(rs.getDouble("confidence"));
					String selectPremise = "SELECT * FROM premise WHERE r_id = ?";
					ps = con.prepareStatement(selectPremise);
					ps.setLong(1, r_id);
					rs_p = ps.executeQuery();
					while(rs_p.next()){
						r.addPremise(rs_p.getString("p_word"));
					}
					DbUtil.close(rs_p);
					DbUtil.close(ps);
					String selectConclusion = "SELECT * FROM conclusion WHERE r_id = ?";
					ps = con.prepareStatement(selectConclusion);
					ps.setLong(1, r_id);
					rs_c = ps.executeQuery();
					while(rs_c.next()){
						r.addConclusion(rs_c.getString("c_word"));
					}
					DbUtil.close(rs_c);
					DbUtil.close(ps);
					rules.add(r);
				}				
			} catch (SQLException e) {
				e.printStackTrace();
			}finally {
				DbUtil.close(rs);
				DbUtil.close(rs_c);
				DbUtil.close(rs_p);
				DbUtil.close(ps);
				DbUtil.close(stmt);
			}
		}else{
			System.out.println("Need an open connection");
			return null;
		}
		return rules;
	}

}
